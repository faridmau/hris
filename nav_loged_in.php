<!-- Horizontal navigation-->
  <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
    <!-- Horizontal menu content-->
    <div data-menu="menu-container" class="navbar-container main-menu-content">
      <!-- include ../../../includes/mixins-->
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
        <li data-menu="" class=" nav-item">
          <a href="index.php" data-toggle="" class="nav-link"><i class="ft-home"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-wrench"></i><span>Administration</span></a>
          <ul class="dropdown-menu">
            <li class="">
              <a href="?page=user" data-toggle="dropdown" class="dropdown-item">Users</a>
            </li>
            <li class="">
              <a href="?page=role" data-toggle="dropdown" class="dropdown-item">Roles</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Menu</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Settings</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Company Structure</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Job Detail Setup</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Qualification Setup</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Training Setup</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Leave Setting</a>
            </li>
            <li class="">
              <a href="?page=menu" data-toggle="dropdown" class="dropdown-item">Company Loans</a>
            </li>
          </ul>
        </li>
        <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-th-large"></i><span>Master</span></a>
          <ul class="dropdown-menu">
            <li class="">
              <a href="#" data-toggle="dropdown" class="dropdown-item">Division</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Position</a>
            </li>
          </ul>
        </li>
        <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-money"></i><span>Payroll</span></a>
          <ul class="dropdown-menu">
            <li class="">
              <a href="#" data-toggle="dropdown" class="dropdown-item">Salary</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Payroll Report</a>
            </li>
          </ul>
        </li>
        <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-user-circle"></i><span>Employees</span></a>
          <ul class="dropdown-menu">
            <li class="">
              <a href="#" data-toggle="dropdown" class="dropdown-item">Employees</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Employee History</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Document Management</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Expenses Administration</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Monitor Attendances</a>
            </li>
          </ul>
        </li>
        <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-money"></i><span>Finances</span></a>
          <ul class="dropdown-menu">
            <li class="">
              <a href="#" data-toggle="dropdown" class="dropdown-item">Expenses</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Salary</a>
            </li>
            <li class="">
              <a href="?page=position" data-toggle="dropdown" class="dropdown-item">Loans</a>
            </li>
          </ul>
        </li>
        <li data-menu="" class=" nav-item"><a href="full-calendar.php"  class="nav-link"><i class="fa fa-calendar"></i><span>Calendar</span></a>
        </li>
        <li data-menu="dropdown" class="dropdown nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link"><i class="fa fa-plane"></i><span>Travel</span></a>
          <ul class="dropdown-menu">
            <li class="">
              <a href="#" data-toggle="dropdown" class="dropdown-item">Travel</a>
            </li>
          </ul>
        </li>
        <li data-menu="" class="nav-item"><a href="report.php" data-toggle="" class="nav-link"><i class="fa fa-file-excel-o"></i><span>Attendances</span></a>
        </li>
        <li data-menu="" class="nav-item"><a href="auth.php" data-toggle="" class="nav-link"><i class="fa fa-sign-in"></i><span>Auth Page</span></a>
        </li>
      </ul>
    </div>
    <!-- /horizontal menu content-->
  </div>
