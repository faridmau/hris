$(function() {
    $('.refresh').on('click',function(){
      $(".dataTable").DataTable().ajax.reload();
    });

    $('input[type="checkbox"]').change(function(){
      if ($(this).prop('checked')){
        $(this).val(1);
      } else {
        $(this).val(0);
      }
    });

    $('.toSlug').on('change keyup',function(){
        var _this = $(this).val();
        $('.fromSlug').val(convertToSlug(_this));
    });
});

/*
    **********************
    Start Generic Function
    **********************
*/
function resetSelectBox(idElement) {
    $('#'+idElement).find('option').remove().end().append('<option value="">- Please Select  -</option>').val('');
}

function appendSelectBox(objectData, idElement) {
    $.each(objectData, function(key, value) {
        $('#province_id').append('<option value="'+key+'">'+value+'</option>');
    });
}

/*
    **********************
    End Generic Function
    **********************
*/