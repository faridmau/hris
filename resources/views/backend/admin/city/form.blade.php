<div class="form-body">
  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Country</label>
    <div class="col-md-9">
      @if($city)
        {!! Form::select( 'country_id', $countries, null, [ 'class' => 'form-control country' , 'required' => true] ) !!}
      @else
        {!! Form::select( 'country_id', ['' => '-- Select Country --'] + $countries, null, [ 'class' => 'form-control country' , 'required' => true] ) !!}
      @endif

    	{!! Form::errorMsg('country_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Region</label>
    <div class="col-md-9">
        @if($city)
          {!! Form::select( 'region_id', $regions, null, [ 'class' => 'form-control region' , 'required' => true] ) !!}
        @else
          <select name="region_id" class="form-control region" required>
            <option value="">-- Select Region --</option>
          </select>
        @endif
    	{!! Form::errorMsg('country_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Name</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('name') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Is Active</label>
    <div class="col-md-9">
      <fieldset>
        <label class="custom-control custom-checkbox">
          {!! Form::checkbox( 'is_active', (!empty( $city ) ? $city->is_active : 0), null,['class' => 'custom-control-input']) !!}
          <span class="custom-control-indicator"></span>
        </label>
      </fieldset>
    </div>
  </div>
</div>

<div class="form-actions right">
  <a href="{{ route('admin.city.index') }}"><button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> {{ trans('general.back') }}</button></a>
  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
</div>

@push( 'scripts' )
<script type="text/javascript">
  $( document ).ready(function() {
    $(document).on('change','.country',function(){
      var _val = $(this).val();
      var _url = '{!! route("admin.city.ajax") !!}';
      if (_val != '') {
        $.get(_url,{country_id:_val},function(result){
          if (Object.keys(result).length > 0) {
            var html = '<option value="">-- Select Region --</option>';
            $.each(result,function(key,value){
              html += '<option value="'+key+'">'+value+'</option>';
            });
            $('.region').html(html);
          }
        });
      }
    });
  });
</script>
@endpush
