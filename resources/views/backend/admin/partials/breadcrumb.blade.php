<div class="row breadcrumbs-top">
  <div class="breadcrumb-wrapper col-12">
    <ol class="breadcrumb">
        <?php
        $segments = Request::segments();
        ?>
        @foreach ($segments as $key => $segment)
            @if($segment !== 'admin')
                <li class="breadcrumb-item {{ $key+1 == count($segments) ? 'active':''}}">
                  @if($key+1 == count($segments))
                    {{ucwords(str_replace('-', ' ', $segment))}}
                  @else
                    <a href="">{{ucwords(str_replace('-', ' ', $segment))}}</a>
                  @endif
                </li>
            @endif
        @endforeach
    </ol>
  </div>
</div>
