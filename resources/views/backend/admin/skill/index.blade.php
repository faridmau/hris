@extends('backend.layouts.admin.master')

@section('title')
Skill
@endsection

@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<!-- END VENDOR CSS-->
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Skill Management</h3>
      </div>
      <div class="content-header-right col-md-6 col-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
          <a href="{{ route('admin.skill.create') }}" class="btn btn-secondary"><i class="ft-plus"></i>Add</a>
        </div>
      </div>
    </div>
    <div class="content-body">
      @include( 'flash::message' )
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  <table class="table table-striped table-bordered" id="skill-table">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection

@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

<script type="text/javascript">
$( document ).ready(function() {
  /********************************
  *       `new` constructor       *
  ********************************/
  var data = $( '#skill-table' ).DataTable({
    processing: true,
    serverSide: true,
    ajax: "{!! route( 'admin.skill.getdata' ) !!}",
    columns: [
      { data: 'name', name: 'name' },
      { data: 'action', name: 'action', class: 'text-center', searchable: false, orderable: false }
    ]
  });

  new $.fn.dataTable.Responsive(data);
});
</script>

@include('backend.admin.forms.delete-modal')
@endpush
