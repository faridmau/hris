@extends('backend.layouts.admin.master')

@section('title')
Dashboard
@endsection

@section('plugin-page')
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/charts/jquery-jvectormap-2.0.3.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/pages/timeline.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/charts/morris.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/unslider.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/weather-icons/climacons.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/plugins/calendars/fullcalendar.css">
@endsection

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Stats -->
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-primary bg-darken-2">
                                    <i class="fa fa-calendar font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-primary white media-body">
                                    <h5>Agenda</h5>
                                    <h5 class="text-bold-400 mb-0"><i class="ft-plus"></i> 28</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-danger bg-darken-2">
                                    <i class="icon-user font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-danger white media-body">
                                    <h5>New Employees</h5>
                                    <h5 class="text-bold-400 mb-0">2</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-warning bg-darken-2">
                                    <i class="fa fa-question-circle-o font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-warning white media-body">
                                    <h5>New Approval</h5>
                                    <h5 class="text-bold-400 mb-0">4</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-success bg-darken-2">
                                    <i class="icon-wallet font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-success white media-body">
                                    <h5>Total Employees</h5>
                                    <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i> 100</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Stats -->
            <!--Product sale & buyers -->
            <div class="row match-height">
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Recent Employees</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content px-1">
                            <div id="recent-buyers" class="media-list height-300 position-relative">
                                <a href="#" class="media border-0">
                                    <div class="media-left pr-1">
                                        <span class="avatar avatar-md avatar-online">
                                        <img class="media-object rounded-circle" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-7.png"
                                            alt="Generic placeholder image">
                                        <i></i>
                                        </span>
                                    </div>
                                    <div class="media-body w-100">
                                        <h6 class="list-group-item-heading">Kristopher Candy
                                            <span class="font-small-3 float-right pt-1">2 January 2018</span>
                                        </h6>
                                        <p class="list-group-item-text mb-0">
                                            <span class="badge badge-primary">Marketing</span>
                                            <span class="badge badge-warning ml-1">Staff</span>
                                        </p>
                                    </div>
                                </a>
                                <a href="#" class="media border-0">
                                    <div class="media-left pr-1">
                                        <span class="avatar avatar-md avatar-away">
                                        <img class="media-object rounded-circle" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-8.png"
                                            alt="Generic placeholder image">
                                        <i></i>
                                        </span>
                                    </div>
                                    <div class="media-body w-100">
                                        <h6 class="list-group-item-heading">Lawrence Fowler
                                            <span class="font-small-3 float-right pt-1">2 January 2018</span>
                                        </h6>
                                        <p class="list-group-item-text mb-0">
                                            <span class="badge badge-danger">Appliances</span>
                                            <span class="badge badge-warning ml-1">Staff</span>
                                        </p>
                                    </div>
                                </a>
                                <a href="#" class="media border-0">
                                    <div class="media-left pr-1">
                                        <span class="avatar avatar-md avatar-busy">
                                        <img class="media-object rounded-circle" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-9.png"
                                            alt="Generic placeholder image">
                                        <i></i>
                                        </span>
                                    </div>
                                    <div class="media-body w-100">
                                        <h6 class="list-group-item-heading">Linda Olson
                                            <span class="font-small-3 float-right pt-1">2 January 2018</span>
                                        </h6>
                                        <p class="list-group-item-text mb-0">
                                            <span class="badge badge-primary">IT</span>
                                            <span class="badge badge-success ml-1">Staff</span>
                                        </p>
                                    </div>
                                </a>
                                <a href="#" class="media border-0">
                                    <div class="media-left pr-1">
                                        <span class="avatar avatar-md avatar-online">
                                        <img class="media-object rounded-circle" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-10.png"
                                            alt="Generic placeholder image">
                                        <i></i>
                                        </span>
                                    </div>
                                    <div class="media-body w-100">
                                        <h6 class="list-group-item-heading">Roy Clark
                                            <span class="font-small-3 float-right pt-1">2 January 2018</span>
                                        </h6>
                                        <p class="list-group-item-text mb-0">
                                            <span class="badge badge-warning">Finance</span>
                                            <span class="badge badge-danger ml-1">Manager</span>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Recent Approvals</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <p>Total approval 10, need confirmation 4.
                                    <span class="float-right"><a href="project-summary.html" target="_blank">Detail Approvals <i class="ft-arrow-right"></i></a></span>
                                </p>
                            </div>
                            <div class="table-responsive">
                                <table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Division</th>
                                            <th>Employee Name</th>
                                            <th>Status</th>
                                            <th>Total Request Day</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-truncate">23 January 2017</td>
                                            <td class="text-truncate"><a href="#">Finance</a></td>
                                            <td class="text-truncate">Elizabeth W.</td>
                                            <td class="text-truncate">
                                                <span class="badge badge-default badge-success">Approved</span>
                                            </td>
                                            <td class="text-truncate">3</td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate">23 January 2017</td>
                                            <td class="text-truncate"><a href="#">Marketing</a></td>
                                            <td class="text-truncate">Doris R.</td>
                                            <td class="text-truncate">
                                                <span class="badge badge-default badge-warning">Waiting</span>
                                            </td>
                                            <td class="text-truncate">2</td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate">23 January 2017</td>
                                            <td class="text-truncate"><a href="#">HR</a></td>
                                            <td class="text-truncate">Andrew D.</td>
                                            <td class="text-truncate">
                                                <span class="badge badge-default badge-success">Approved</span>
                                            </td>
                                            <td class="text-truncate">3</td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate">23 January 2017</td>
                                            <td class="text-truncate"><a href="#">IT</a></td>
                                            <td class="text-truncate">Megan S.</td>
                                            <td class="text-truncate">
                                                <span class="badge badge-default badge-danger">Rejected</span>
                                            </td>
                                            <td class="text-truncate">1</td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate">PO-32521</td>
                                            <td class="text-truncate"><a href="#">INV-008101</a></td>
                                            <td class="text-truncate">Walter R.</td>
                                            <td class="text-truncate">
                                                <span class="badge badge-default badge-warning">Waiting</span>
                                            </td>
                                            <td class="text-truncate">4</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Product sale & buyers -->
            <!-- Basic Horizontal Timeline -->
            <div class="row match-height">
                <div class="col-xl-8 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">My Calendar</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-text">
                                    <section class="cd-horizontal-timeline">
                                        <div class="timeline">
                                            <div class="events-wrapper">
                                                <div class="events">
                                                    <ol>
                                                        <li><a href="#0" data-date="16/01/2015" class="selected">16 Jan</a></li>
                                                        <li><a href="#0" data-date="28/02/2015">28 Feb</a></li>
                                                        <li><a href="#0" data-date="20/04/2015">20 Mar</a></li>
                                                        <li><a href="#0" data-date="20/05/2015">20 May</a></li>
                                                        <li><a href="#0" data-date="09/07/2015">09 Jul</a></li>
                                                        <li><a href="#0" data-date="30/08/2015">30 Aug</a></li>
                                                        <li><a href="#0" data-date="15/09/2015">15 Sep</a></li>
                                                    </ol>
                                                    <span class="filling-line" aria-hidden="true"></span>
                                                </div>
                                                <!-- .events -->
                                            </div>
                                            <!-- .events-wrapper -->
                                            <ul class="cd-timeline-navigation">
                                                <li><a href="#0" class="prev inactive">Prev</a></li>
                                                <li><a href="#0" class="next">Next</a></li>
                                            </ul>
                                            <!-- .cd-timeline-navigation -->
                                        </div>
                                        <!-- .timeline -->
                                        <div class="events-content">
                                            <ol>
                                                <li class="selected" data-date="16/01/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-5.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                                <li data-date="28/02/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-6.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                                <li data-date="20/04/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-7.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                                <li data-date="20/05/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-8.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                                <li data-date="09/07/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-9.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                                <li data-date="30/08/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-6.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                                <li data-date="15/09/2015">
                                                    <blockquote class="blockquote border-0">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img class="media-object img-xl mr-1" src="{{asset('assets/backend')}}/images/portrait/small/avatar-s-7.png"
                                                                    alt="Generic placeholder image">
                                                            </div>
                                                            <div class="media-body">
                                                                Sometimes life is going to hit you in the head with a brick. Don't lose faith.
                                                            </div>
                                                        </div>
                                                        <footer class="blockquote-footer text-right">Steve Jobs
                                                            <cite title="Source Title">Entrepreneur</cite>
                                                        </footer>
                                                    </blockquote>
                                                    <p class="lead mt-2">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia,
                                                        fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur
                                                        aspernatur at.
                                                    </p>
                                                </li>
                                            </ol>
                                        </div>
                                        <!-- .events-content -->
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Basic Card</h4>
                        </div>
                        <div class="card-content">
                            <img class="img-fluid" src="{{asset('assets/backend')}}/images/carousel/06.jpg" alt="Card image cap">
                            <div class="card-body">
                                <p class="card-text">Some quick example text to build on the card title and make up
                                    the bulk of the card's content.
                                </p>
                                <a href="#" class="card-link">Card link</a>
                                <a href="#" class="card-link">Another link</a>
                            </div>
                        </div>
                        <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                            <span class="float-left">3 hours ago</span>
                            <span class="float-right">
                            <a href="#" class="card-link">Read More <i class="fa fa-angle-right"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Basic Horizontal Timeline -->
        </div>
    </div>
</div>

@endsection

@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/extensions/jquery.knob.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/extensions/knob.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/charts/raphael-min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/charts/morris.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/charts/jvector/jquery-jvectormap-2.0.3.min.js"
<script src="{{ asset( 'assets/backend')}}/js/Pvendors/js/timeline/horizontal-timeline.js"
type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/charts/jvector/jquery-jvectormap-world-mill.js"
type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/jvector/visitor-data.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/charts/chart.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/charts/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/extensions/unslider-min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/colors/palette-climacon.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/fonts/simple-line-icons/style.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/js/scripts/extensions/fullcalendar.js">
<!-- END PAGE VENDOR JS-->

<!-- BEGIN PAGE LEVEL JS-->
<script src="{{ asset( 'assets/backend')}}/js/pages/dashboard-analytics.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
@endpush
