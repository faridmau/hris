<div class="form-body">
  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Country</label>
    <div class="col-md-9">
      {!! Form::select( 'country_id', $countries, null, [ 'class' => 'form-control' ] ) !!}
    	{!! Form::errorMsg('country_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Name</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('name') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Is Active</label>
    <div class="col-md-9">
      <fieldset>
        <label class="custom-control custom-checkbox">
          {!! Form::checkbox( 'is_active', (!empty( $region ) ? $region->is_active : 0), null,['class' => 'custom-control-input']) !!}
          <span class="custom-control-indicator"></span>
        </label>
      </fieldset>
    </div>
  </div>
</div>

<div class="form-actions right">
  <a href="{{ route('admin.region.index') }}"><button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> {{ trans('general.back') }}</button></a>
  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
</div>

@push( 'scripts' )
@endpush
