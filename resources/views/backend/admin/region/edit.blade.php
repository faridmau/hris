@extends('backend.layouts.admin.master')

@section('title')
Edit Region
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Region Management</h3>
      </div>
    </div>
    <div class="content-body">
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  {!! Form::model($region, ['route'=>['admin.region.update', $region->id], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-region']) !!}
      							@include('backend.admin.region.form')
      						{!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection
