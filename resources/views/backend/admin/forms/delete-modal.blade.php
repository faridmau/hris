<div class="modal fade text-left" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10"
a1ria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <h4 class="modal-title" id="myModalLabel10">Delete Confirmation</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure want to delete this data?</p>
      </div>
      <div class="modal-footer">
        {!! Form::open(['id' => 'destroy', 'method' => 'DELETE']) !!}
        <a id="delete-modal-cancel" href="#" class="btn grey btn-outline-secondary" data-dismiss="modal">Cancel</a>
        {!! Form::submit('Delete', [ 'class' => 'btn btn-outline-danger' ]) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {

  $(document).on('click', '#deleteModal', function(e) {
    var url = $(this).attr('data-href');
    $('#destroy').attr('action', url );
    $('#import').attr( 'method', 'delete' );
    $('#delete-modal').modal('show');
    e.preventDefault();
  });
});
</script>
