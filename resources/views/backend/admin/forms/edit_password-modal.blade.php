<div class="modal fade text-left" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10"
a1ria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger white">
        <h4 class="modal-title" id="myModalLabel10">Reset Password Confirmation</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure want to reset password this data?</p>
        {!! Form::open(['id' => 'edit', 'method' => 'PUT']) !!}
        <div class="form-group row">
            <label class="col-md-3 label-control" for="timesheetinput1">Password</label>
            <div class="col-md-9">
                <div class="position-relative">
                {!! Form::password( 'password', [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-3 label-control" for="timesheetinput1">Password Confirmation</label>
            <div class="col-md-9">
                <div class="position-relative">
                {!! Form::password( 'password_confirmation', [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <a id="delete-modal-cancel" href="#" class="btn grey btn-outline-secondary" data-dismiss="modal">Cancel</a>
        {!! Form::submit('Reset', [ 'class' => 'btn btn-outline-success' ]) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  $(document).on('click', '#editPasswordModal', function(e) {
    var url = $(this).attr('href');
    $('#edit').attr('action', url );
    $('#edit-modal').modal('show');
    e.preventDefault();
  });

  $(document).on('submit','form#edit', function(e) {
    e.preventDefault();
      $.ajax({
        url: $('form#edit').attr('action'),
        type: 'PUT',
        data: $('form#edit').serialize(),
        success: function(data) {
          if(data.messages) {
            toastr.warning(data.messages, 'Info');
            $('input[name="password_confirmation"]').val('');
            $('input[name="password"]').val('');
          } else {
            toastr.success('Reset Password success !', 'Info');
            $('#edit-modal').modal('hide');
            $('input[name="password_confirmation"]').val('');
            $('input[name="password"]').val('');
          }
        }
      });
  });
});
</script>
