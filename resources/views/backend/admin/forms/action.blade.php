<div class="btn-group hidden-xs" id="exampleToolbar" role="group">
    @if( isset( $show_url ) )
    <a href="javascript:void(0)" class="btn btn-info btn-icon waves-effect waves-light {!! empty( $show_url ) ? 'disabled' : '' !!}" title="Show" data-button="edit">
    <i class="fa fa-search fa-fw"></i>
    </a>
    @endif
    @if( isset( $edit_url ) )
    <a href="javascript:void(0)" class="{!! empty( $edit_url ) ? 'disabled' : '' !!} btn btn-icon btn-primary btn-edit-{{ $id }}" title="Edit" data-id={{ $id }} data-button="edit" onClick="editData({{ $id }})" data-href="{{ $edit_url }}">
    <i class="fa fa-edit" aria-hidden="true"></i>
    </a>
    @endif
    @if( isset( $edit_password_url ) )
    <a href="javascript:void(0)" class="{!! empty( $edit_password_url ) ? 'disabled' : '' !!} btn btn-icon btn-info" title="Edit Password" data-button="edit" id="editPasswordModal">
    <i class="fa fa-key" aria-hidden="true"></i>
    </a>
    @endif
    @if( isset( $delete_url ) )
    <a href="javascript:void(0)" class="{!! empty( $delete_url ) ? 'disabled' : '' !!} btn btn-icon btn-danger" title="Delete" data-href="{!! empty( $delete_url ) ? 'javascript:void(0)' : $delete_url !!}" data-button="delete" id="deleteModal">
    <i class="fa fa-trash-o" aria-hidden="true"></i>
    </a>
    @endif
    @if( isset( $edit_url_ajax ) )
    <a href="javascript:void(0)" class="edit-url-ajax" title="Edit" data-button="edit" data-href="{{ $edit_url_ajax }}">
    <button type="button" class="btn btn-icon btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></button>
    </a>
    @endif
    @if( isset( $delete_url_ajax ) )
    <a href="javascript:void(0)" class="delete-url-ajax" title="Delete" data-href="{!! empty( $delete_url_ajax ) ? 'javascript:void(0)' : $delete_url_ajax !!}">
    <button type="button" class="btn btn-icon btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
    </a>
    @endif
    @if( isset( $print_url ) )
    <a href="javascript:void(0)" class="print-url" title="Print" data-button="print" data-href="{{ $print_url }}">
    <button type="button" class="btn btn-icon btn-warning"><i class="fa fa-print" aria-hidden="true"></i></button>
    </a>
    @endif
</div>