@extends('backend.layouts.admin.master')

@section('title')
Profile
@endsection

@section('plugin_page')
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/pages/users.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/pages/timeline.css"">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/cropper/cropper.css">
@endsection
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
          @include( 'flash::message' )
            <div id="user-profile">
                <div class="row">
                    <div class="col-12">
                        <div class="card profile-with-cover">
                            <div class="card-img-top img-fluid bg-cover height-300" style="background: url('{{ Sentinel::getUser()->getCover() }}') 50%;">
                            </div>
                            <div class="media profil-cover-details w-100">
                                <div class="media-left pl-2 pt-2">
                                    <a href="{{ Sentinel::getUser()->getAvatar() }}" class="profile-image">
                                        <img src="{{ Sentinel::getUser()->getAvatar() }}" class="rounded-circle img-border height-100 width-100" alt="Card image">
                                    </a>
                                </div>
                                <div class="media-body pt-3 px-2">
                                    <div class="row">
                                        <div class="col">
                                            <h3 class="card-title">{{ Sentinel::getUser()->name }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav class="navbar navbar-light navbar-profile align-self-end">
                                <nav class="navbar navbar-expand-lg">                                    
                                </nav>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                        <h4 class="card-title">Profile</h4>
                          <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            </ul>
                        </div>
                      </div>      
                      <div class="card-content collapse show">
                        <div class="card-body">
                          <ul class="nav nav-tabs nav-underline">
                            <li class="nav-item">
                              <a class="nav-link active" id="tab-activity" data-toggle="tab" aria-controls="tab-icon-activity"
                              href="#tab-icon-activity" aria-expanded="true"><i class="ft-bar-chart-2"></i> Activity</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="tab-update-user" data-toggle="tab" aria-controls="tab-icon-update-user"
                              href="#tab-icon-update-user" aria-expanded="false"><i class="ft-user"></i> Update User</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="tab-update-password" data-toggle="tab" aria-controls="tab-icon-update-password"
                              href="#tab-icon-update-password" aria-expanded="false"><i class="ft-lock"></i> Update Password</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="tab-update-images" data-toggle="tab" aria-controls="tab-icon-update-images"
                              href="#tab-icon-update-images" aria-expanded="false"><i class="ft-gitlab"></i> Update Avatar/Cover</a>
                            </li>
                          </ul>
                          <div class="tab-content px-1 pt-1">
                            <div role="tabpanel" class="tab-pane active" id="tab-icon-activity" aria-expanded="true"
                            aria-labelledby="tab-activity">
                              <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="activity-table">
                                  <thead>
                                    <tr>
                                      <th>Activity</th>
                                      <th>Date</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($activity as $act)
                                    <tr>
                                      <td>{{ $act->description }}</td>
                                      <td>{{ $act->created_at }}</td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="tab-pane" id="tab-icon-update-user" aria-labelledby="tab-update-user">
                              {!! Form::model($id, ['route'=>['profile.update_user'], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}
                                @include('backend.admin.user.form',['tipe'=> ''])
                              {!! Form::close() !!}
                            </div>
                            <div class="tab-pane" id="tab-icon-update-password" aria-labelledby="tab-update-password">
                              {!! Form::model($id, ['route'=>['profile.update_password'], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}
                                @include('backend.admin.user.form',['tipe' => 'edit_password'])
                              {!! Form::close() !!}
                            </div>
                            <div class="tab-pane" id="tab-icon-update-images" aria-labelledby="tab-update-images">
                              {!! Form::model($id, ['route'=>['profile.update_images'], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}  
                                @include('backend.admin.user.form',['tipe' => 'edit_images'])
                              {!! Form::close() !!}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>  
<!-- END PAGE VENDOR JS-->

<script type="text/javascript">
  $(function() {
    $('#activity-table').DataTable();
  });
</script>
@endpush