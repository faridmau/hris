@extends('backend.layouts.admin.master')

@section('title')
Edit Event Type
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Event Type Management</h3>
      </div>
    </div>
    <div class="content-body">
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  {!! Form::model($event_type, ['route'=>['admin.event_type.update', $event_type->id], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-event_type']) !!}
      							@include('backend.admin.event_type.form')
      						{!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection
