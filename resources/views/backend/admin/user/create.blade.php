@extends('backend.layouts.admin.master')

@section('title')
Create User
@endsection

@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/cropper/cropper.css">
<!-- END VENDOR CSS-->
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">User Management</h3>
      </div>
    </div>
    <div class="content-body">
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  {!! Form::open( [ 'route' => 'admin.user.store', 'method' => 'post', 'id' => 'create-menu','enctype' => 'multipart/form-data' ] ) !!}
                    @include( 'backend.admin.user.form' )
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection