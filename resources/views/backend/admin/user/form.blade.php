@if(empty($tipe)) @php $tipe = ''; @endphp @endif
<div class="form-body">
  @if (\Request::route()->getName() == "profile.edit_images" || $tipe == 'edit_images')
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Avatar</label>
    <div class="col-md-9">
      <div class="img-container overflow-hidden">
        <img class="image-avatar img-fluid cropper-hidden" src="" alt="Picture">
      </div>
      <div class="position-relative">
        {!! Form::file( 'avatar', [ 'class' => 'form-control empty avatar-crop' ] ) !!}
        <input type="hidden" name="avatar-crop-data">
        {!! Form::errorMsg('avatar') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Cover Page</label>
    <div class="col-md-9">
      <div class="img-container overflow-hidden">
        <img class="image-cover img-fluid cropper-hidden" src="" alt="Picture">
      </div>
      <div class="position-relative">
        {!! Form::file( 'cover', [ 'class' => 'form-control empty cover-crop' ] ) !!}
        <input type="hidden" name="cover-crop-data">
        {!! Form::errorMsg('cover') !!}
      </div>
    </div>
  </div>
</div>  
  @elseif(\Request::route()->getName() == "profile.edit_password" || $tipe == 'edit_password')
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Password</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::password( 'password', [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('password') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Password Confirmation</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::password( 'password_confirmation', [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('password_confitmation') !!}
      </div>
    </div>
  </div>
</div>
  @else
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Email</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::email( 'email', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true, (!empty($id)) ? 'readonly' : '' ] ) !!}
        {!! Form::errorMsg('email') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Name</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('name') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Username</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'username', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('username') !!}
      </div>
    </div>
  </div>
 @if (\Request::route()->getName() == "admin.user.create")
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Password</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::password( 'password', [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('password') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Password Confirmation</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::password( 'password_confirmation', [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('password_confitmation') !!}
      </div>
    </div>
  </div>
  @endif
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Phone</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::number( 'phone', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off' ] ) !!}
        {!! Form::errorMsg('phone') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Gender</label>
    <div class="col-md-9">
        @if(!empty($id))
            <select class="form-control" name="gender">
            <option value>- Select Gender -</option>
            <option value="Men" {{ $id->gender == "Men" ? 'selected' : '' }} > Men </option>
            <option value="Women" {{ $id->gender != "Men" ? 'selected' : '' }}> Women </option>
            </select>
        @else
            {!! Form::select( 'gender', [ '' => '- Select Gender -' ,'Men' => 'Men', 'Women' => 'Women' ], null, [ 'class' => 'form-control' ] ) !!}
        @endif

        {!! Form::errorMsg('gender') !!}
    </div>
  </div>
  @if(\Request::route()->getName() == "admin.user.edit" || \Request::route()->getName() == "admin.user.create")
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Avatar</label>
    <div class="col-md-9">
      <div class="img-container overflow-hidden">
        <img class="image-avatar img-fluid cropper-hidden" src="" alt="Picture">
      </div>
      <div class="position-relative">
        {!! Form::file( 'avatar', [ 'class' => 'form-control empty avatar-crop' ] ) !!}
        <input type="hidden" name="avatar-crop-data">
        {!! Form::errorMsg('avatar') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Cover Page</label>
    <div class="col-md-9">
      <div class="img-container overflow-hidden">
        <img class="image-cover img-fluid cropper-hidden" src="" alt="Picture">
      </div>
      <div class="position-relative">
        {!! Form::file( 'cover', [ 'class' => 'form-control empty cover-crop' ] ) !!}
        <input type="hidden" name="cover-crop-data">
        {!! Form::errorMsg('cover') !!}
      </div>
    </div>
  </div>
  
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Role</label>
    <div class="col-md-9">
        @if(!empty($id))
            <select class="form-control" name="role" required="required">
                @foreach ($roles as $slug => $rolename)
                    <option value="{{ $slug }}" {{ ($id->roles()->first()->slug == $slug) ? 'selected' : '' }}>{{ $rolename }}</option>
                @endforeach
            </select>
        @else
            {!! Form::select( 'role', $roles, null, [ 'class' => 'form-control', 'required' => true ] ) !!}
        @endif

        {!! Form::errorMsg('role') !!}
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Is Active</label>
    <div class="col-md-9">
      <fieldset>
        <label class="custom-control custom-checkbox">
          {!! Form::checkbox( 'is_active', ((!empty( $id->is_active ) || !empty(old('is_active'))) ? 1 : 0), null,['class' => 'custom-control-input']) !!}
          <span class="custom-control-indicator"></span>
        </label>
      </fieldset>
    </div>
  </div>
  @endif
</div>
@endif
<div class="form-actions right">
  <a href="{{ url()->previous() }}"><button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> {{ trans('general.back') }}</button></a>
  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
</div>
@if(empty($tipe))
@push( 'scripts' )
<script src="{{ asset( 'assets/backend')}}/js/vendors/croppers/cropper.min.js" type="text/javascript"></script>

<script type="text/javascript">
  var image = $('.image-avatar');  
  var image_cover = $('.image-cover');  
  var options = {
    viewMode: 1,
    dragMode: 'move',
    aspectRatio: 1/1,
    autoCropArea: 0.5,
    restore: false,
    zoomOnWheel: true,
    background: false,
    cropBoxMovable: false,
    cropBoxResizable: false
  }
  var optionsCover = {
    viewMode: 1,
    dragMode: 'move',
    aspectRatio: 11/3,
    autoCropArea: 1,
    restore: false,
    zoomOnWheel: true,
    background: false,
    cropBoxMovable: false,
    cropBoxResizable: false
  }
  image.cropper(options);
  image_cover.cropper(optionsCover);

  $('.avatar-crop').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      if (/^image\/\w+/.test(this.files[0].type)) {
        reader.onload = function(e) {
          $('.image-avatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

        image.cropper('destroy');
        setTimeout(function() {
          image.cropper(options);
        },100);
      } else {
        image.cropper('destroy');
        $('.image-avatar').removeAttr('src');
        $('.image-avatar').addClass('cropper-hidden');
      }
    }
  });

  $('.cover-crop').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      if (/^image\/\w+/.test(this.files[0].type)) {
        reader.onload = function(e) {
          $('.image-cover').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]);

        image_cover.cropper('destroy');
        setTimeout(function() {
          image_cover.cropper(optionsCover);
        },100);
      } else {
        image_cover.cropper('destroy');
        $('.image-cover').removeAttr('src');
        $('.image-cover').addClass('cropper-hidden');
      }
    }
  });

  $('.cover-crop').click(function() {
    document.body.onfocus = clearCoverIfNull;
  });

  function clearCoverIfNull()
  {
    if ($('.cover-crop').get(0).files.length == 0) {
      image_cover.cropper('destroy');
      $('.image-cover').removeAttr('src');
      $('.image-cover').addClass('cropper-hidden');
      document.body.onfocus = null
    }
  }

  $('.avatar-crop').click(function() {
    document.body.onfocus = clearAvatarIfNull;
  });

  function clearAvatarIfNull()
  {
    if ($('.avatar-crop').get(0).files.length == 0) {
      image.cropper('destroy');
      $('.image-avatar').removeAttr('src');
      $('.image-avatar').addClass('cropper-hidden');
      document.body.onfocus = null
    }
  }

  $('form').submit(function(e) {
    if ($('.cover-crop').get(0).files.length > 0) $('input[name="cover-crop-data"]').val(JSON.stringify(image_cover.cropper('getData')));

    if($('.avatar-crop').get(0).files.length >0) $('input[name="avatar-crop-data"]').val(JSON.stringify(image.cropper('getData')));
  });
</script>
@endpush
@endif