@extends('backend.layouts.admin.master')

@section('title')
Edit User
@endsection

@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/cropper/cropper.css">
<!-- END VENDOR CSS-->
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">User Management</h3>
      </div>
    </div>
    <div class="content-body">
      @include( 'flash::message' )
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  @if(\Request::route()->getName() == "profile.edit_user")
                    {!! Form::model($id, ['route'=>['profile.update_user'], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}
                  @elseif(\Request::route()->getName() == "profile.edit_images")
                    {!! Form::model($id, ['route'=>['profile.update_images'], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}  
                  @elseif(\Request::route()->getName() == "profile.edit_password")
                    {!! Form::model($id, ['route'=>['profile.update_password'], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}
                  @else
                    {!! Form::model($id, ['route'=>['admin.user.update', $id->id], 'method' => 'put', 'autocomplete'=>'off', 'id'=>'form-menu','enctype' => 'multipart/form-data']) !!}
                  @endif
                    @include('backend.admin.user.form')
      						{!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection