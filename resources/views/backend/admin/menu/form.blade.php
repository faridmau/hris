<div class="form-body">
  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Type</label>
    <div class="col-md-9">
      @if(!empty($menu))
        <select class="form-control" id="parent">
          <option value="parent" {{ $menu->parent_id == 0 ? 'selected' : '' }} > Parent </option>
          <option value="child" {{ $menu->parent_id != 0 ? 'selected' : '' }}> Child </option>
        </select>
      @else
        {!! Form::select( 'parent', [ '' => '- Select Parent -' ,'parent' => 'Parent', 'child' => 'Child' ], null, [ 'class' => 'form-control', 'required' => true, 'id' => 'parent' ] ) !!}
      @endif

      {!! Form::errorMsg('parent') !!}
    </div>
  </div>

  @php
    if(empty($menu)){
      $class= 'parent_hide';
    }else{
      if($menu->parent_id == 0 ){
        $class= 'parent_hide';
      }else{
        $class= '';
      }
    }
  @endphp

  <div class="form-group row {{ $class }}">
    <label class="col-md-3 label-control" for="parent">Parent</label>
    <div class="col-md-9">
      <select class="form-control" id="type" name="parent_id">
        <option value="">Parent</option>
        @if($parents)
          @foreach( $parents as $parent )
            <option value="{{ $parent->id }}" {{ (!empty($menu) && $menu->parent_id == $parent->id ? 'selected' : '') }}> {{ $parent->name }} </option>
          @endforeach
        @endif
      </select>

      {!! Form::errorMsg('parent_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Name</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('name') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Icon</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'icon', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off' ] ) !!}
        {!! Form::errorMsg('icon') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Url</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'url', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('url') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Is Active</label>
    <div class="col-md-9">
      <fieldset>
        <label class="custom-control custom-checkbox">
          {!! Form::checkbox( 'is_active', (!empty( $menu ) ? $menu->is_active : 0), null,['class' => 'custom-control-input']) !!}
          <span class="custom-control-indicator"></span>
        </label>
      </fieldset>
    </div>
  </div>
</div>

<div class="form-actions right">
  <a href="{{ route('admin.menu.index') }}"><button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> {{ trans('general.back') }}</button></a>
  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
</div>

@push( 'scripts' )
<script type="text/javascript">
$( '.parent_hide' ).hide();
$('#parent').change(function (){
  var parent = $(this).val();
  if (parent == 'child') {
    $(".parent_hide").show();
  } else {
    $(".parent_hide").hide();
  }

});
</script>

@endpush
