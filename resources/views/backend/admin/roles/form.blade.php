<div class="form-group row">
  <label class="col-md-3 label-control" for="timesheetinput1">Name</label>
  <div class="col-md-9">
    <div class="position-relative">
      {!! Form::text( 'name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
      {!! Form::errorMsg('name') !!}
    </div>
  </div>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th>{!! trans( 'general.menu-list') !!}</th>
      <th class="non-user text-center" style="width:50px;">
        @php
        if (isset($role)) {
          $format = json_decode(json_encode($role),true);
          $rolePermission = count($format['permissions']) + 1;
          $roleMenu = count($menu);
        } else {
          $rolePermission = 0;
          $roleMenu = 1;
        }
        @endphp
        <ul style="list-style:none">
          <li>
            <label class="custom-control custom-checkbox">
              @if($rolePermission == $roleMenu)
              {!! Form::checkbox( 'create-all', 1, null,['checked','class' => 'custom-control-input']) !!}
              @else
              {!! Form::checkbox( 'create-all', false, null,['class' => 'custom-control-input']) !!}
              @endif
              <span class="custom-control-indicator"></span>
              <label>
                {!! trans( 'general.select-menu-all' ) !!}
              </label>
            </label>
          </li>
        </ul>
      </th>
    </tr>
  </thead>
  <tbody>
    @foreach( $permissions as $key => $permission )
    @if( $permission['parent_id'] == 0)
    @php
    $childs = define_child($permission['id']);
    if (isset($role)) {
      $permission_val = permission_val($role->id,$permission['slug']);
      $myrole = $role->id;
    } else {
      $permission_val = 0;
      $myrole = 0;
    }
    @endphp
    @if($permission['slug'] != 'dashboard')
    <tr>
      <td>
        <ul>
          <li>
            {{ $permission['name'] }}
            <ul>
              @foreach($childs as $child)
              <li>{{ ucfirst($child->name) }}</a></li>
              @endforeach
            </ul>
          </li>
        </ul>
      </td>
      <td class="non-user text-center">
        <ul style="list-style:none">
          <li>
            <label class="custom-control custom-checkbox">
              {!! Form::checkbox( 'permissions['.$permission['slug'].']', $permission_val , null, [ 'class' => 'create-box parent custom-control-input','data-id' => $permission['slug']]) !!}
              <span class="custom-control-indicator"></span>
                @if($permission_val == 1)
                <label class="badge badge badge-pill badge-success">Active</label>
                @else
                <label class="badge badge badge-pill badge-danger">InActive</label>
                @endif
            </label>
          </li>
          @foreach($childs as $child)
          <li>
            <label class="custom-control custom-checkbox">
              {!! Form::checkbox( 'permissions['.$child->slug.']', permission_val($myrole,$child->slug), null, [ 'class' => 'create-box child-'.$permission['slug'].' custom-control-input' ]) !!}
              <span class="custom-control-indicator"></span>
                @if(permission_val($myrole,$child->slug) == 1)
                <label class="badge badge badge-pill badge-success">Active</label>
                @else
                <label class="badge badge badge-pill badge-danger">InActive</label>
                @endif
            </label>
          </li>
          @endforeach
        </ul>
      </td>
    </tr>
    @endif
    @endif
    @endforeach
  </tbody>
</table>

<div class="form-actions right">
  <a href="{{ route('admin.role.index') }}"><button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> {{ trans('general.back') }}</button></a>
  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
</div>

@push( 'scripts' )
<script type="text/javascript">
$( 'input[name=create-all]' ).on('click',function(){
  $(".create-box").prop('checked', $(this).prop('checked'));
  $(".create-box").each(function(n,v){
    if ($(this).prop('checked')){
      $(this).val(1);
      $(this).parent().find('label').removeClass('badge-danger').addClass('badge badge badge-pill badge-success');
      $(this).parent().find('label').html('Active');
    } else {
      $(this).val(0);
      $(this).parent().find('label').removeClass('badge-success').addClass('badge badge badge-pill badge-danger');
      $(this).parent().find('label').html('InActive');
    }
  });
});

$(document).on('change',".create-box",function(){
  var count_active = 0;
  var count_inactive = 0;
  var active = 0;
  var inactive = 0;
  var base = $(".create-box").length;
  var _this = $(this);
  if (_this.prop('checked')){
    _this.val(1);
    _this.parent().find('label').removeClass('badge-danger').addClass('badge badge badge-pill badge-success');
    _this.parent().find('label').text('Active');

    if (_this.hasClass('parent')) {
      var parent = _this.data('id');
      $(".child-"+parent+"").each(function(){
        $(this).parent().find('label').removeClass('badge-danger').addClass('badge badge badge-pill badge-success');
        $(this).parent().find('label').text('Active');
        $(this).prop('checked',true);
        $(this).val(1);
      });
    }

  } else {
    _this.val(0);
    _this.parent().find('label').removeClass('badge-success').addClass('badge badge badge-pill badge-danger');
    _this.parent().find('label').text('InActive');

    if (_this.hasClass('parent')) {
      var parent = _this.data('id');
      $(".child-"+parent+"").each(function(){
        $(this).parent().find('label').removeClass('badge-success').addClass('badge badge badge-pill badge-danger');
        $(this).parent().find('label').text('InActive');
        $(this).prop('checked',false);
        $(this).val(0);
      });
    }

  }

  $(".create-box").each(function(k){
    if ($(this).parent().find('label').text() == 'Active'){
      active = k;
    }

    if($(this).parent().find('label').text() == 'InActive'){
      inactive = k;
    }

  });

  if (inactive == 0) {
    $( 'input[name=create-all]' ).prop('checked',true);
  } else {
    $( 'input[name=create-all]' ).prop('checked',false);
  }

});
</script>
@endpush
