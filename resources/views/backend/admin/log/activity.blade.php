@extends('backend.layouts.admin.master')

@section('title')
Activity Log
@endsection

@section( 'plugin_page' )
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<!-- END VENDOR CSS-->
@endsection

@section('content')

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Activity Log</h3>
      </div>
    </div>
    <div class="content-body">
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <form name="filter_order">
                <div class="form-body">
                  <div class="row">
                  <div class="col-md-12">
                  <h4 class="form-section"><i class="icon-clipboard4"></i> Filter</h4>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="projectinput5">Date</label>
                        <input type="text" name="date" placeholder="Date" id="input-date" class="form-control datepicker" value="{{date('Y-m-d') }}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="projectinput6">User</label>
                        {!! Form::select( 'user_id', $users, null, [ 'class' => 'form-control'] ) !!}
                      </div>
                    </div>

                    <div class="pull-right" style="margin:10px">
                      <button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                    </div>

                  </div>
                  </div>
                </div>
                <hr>
                <div class="result" style="margin:10px">
                  <table class="table table-bordered table-hover dataTable width-full" id="activity-table">
                    <thead>
                      <tr>
                        <th>User</th>
                        <th>Activity</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

@endsection

@push( 'scripts' )
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

<script type="text/javascript">
var _url = '{!! route("admin.logs.ajax") !!}';
$(document).ready(function($) {
  $(".datepicker").datepicker({
    format : 'yyyy-mm-dd'
  });

  var data = $( '#activity-table' ).DataTable({
     processing: true,
     serverSide: true,
     bDestroy: true,
     bDeferRender: true,
     ordering: false,
     bLengthChange: false,
     bFilter: false,
     bInfo: false,
     destroy: true,
     ajax: _url+"?&type=getDataFilterActivityAll",
     columns: [
       { data: 'name', name: 'users.name'},
       { data: 'description', name: 'activity_log.description'},
       { data: 'ac_date', name: 'activity_log.created_at'},
     ]
   });

   new $.fn.dataTable.Responsive(data);

  $(document).on('click','#button-filter',function(){
      var _form = $('form[name=filter_order]').serialize();
      table = $( '#activity-table' ).DataTable({
        processing: true,
        serverSide: true,
        bDestroy: true,
        bDeferRender: true,
        ordering: false,
        bLengthChange: false,
        bFilter: false,
        bInfo: false,
        destroy: true,
        ajax: _url+"?"+_form+"&type=getDataFilterActivity",
        columns: [
          { data: 'name', name: 'users.name'},
          { data: 'description', name: 'activity_log.description'},
          { data: 'ac_date', name: 'activity_log.created_at'},
        ]
      });

      new $.fn.dataTable.Responsive(table);
    });
});
</script>
@endpush
