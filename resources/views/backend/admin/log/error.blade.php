@extends('backend.layouts.admin.master')

@section('title')
Log Report
@endsection

@section('content')

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Error Log</h3>
      </div>
    </div>
    <div class="content-body">
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <iframe src="{{route('admin.logs.report')}}" frameborder="0" allowfullscreen style="width:100%; height:800px"></iframe>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

@endsection
