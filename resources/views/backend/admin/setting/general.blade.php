@extends('backend.layouts.admin.master')

@section('title')
Setting
@endsection

@section('plugin_page')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<style type="text/css">
  #map {
    height: 500px;
  }
</style>
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Setting Management</h3>
      </div>
    </div>
    <div class="content-body">
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              @include( 'flash::message' )
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  {!! Form::model($setting, ['route'=>['admin.setting.update'], 'method' => 'post', 'autocomplete'=>'off', 'id'=>'form-setting', 'files' => true]) !!}

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">App Name</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::text( 'app_name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
                          {!! Form::errorMsg('app_name') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Email Support</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::email( 'email_support', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
                          {!! Form::errorMsg('email_support') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Phone Support</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::text( 'phone_support', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
                          {!! Form::errorMsg('phone_support') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Tagline</label>
                      <div class="col-md-9">
                        <div id="summernote-tagline" class="position-relative">
                          <div data-plugin="summernote" class="summernote">
                              {!!  !empty( $setting->tagline ) ? htmlspecialchars_decode($setting->tagline) : ''  !!}
                          </div>
                          {!! Form::errorMsg('tagline') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Keywords</label>
                      <div class="col-md-9">
                        <div id="summernote-keywords" class="position-relative">
                          <div  data-plugin="summernote" class="summernote">
                              {!!  !empty( $setting->keywords ) ? htmlspecialchars_decode($setting->keywords) : ''  !!}
                          </div>
                          {!! Form::errorMsg('tagline') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Icon</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::file( 'icon', [ 'class' => 'form-control empty' ] ) !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Logo</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::file( 'logo', [ 'class' => 'form-control empty' ] ) !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Address</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::text( 'address', null, [ 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true ,'id' => 'address-autocomplete'] ) !!}
                          {!! Form::errorMsg('address') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Map</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          <div id="map"></div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Longitude</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::text( 'longitude', null, [ 'class' => 'form-control longitude empty', 'autocomplete' => 'off', 'required' => false ] ) !!}
                          {!! Form::errorMsg('longitude') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="timesheetinput1">Latitude</label>
                      <div class="col-md-9">
                        <div class="position-relative">
                          {!! Form::text( 'lattitude', null, [ 'class' => 'form-control lattitude empty', 'autocomplete' => 'off', 'required' => false ] ) !!}
                          {!! Form::errorMsg('lattitude') !!}
                        </div>
                      </div>
                    </div>

                    <div class="form-actions right">
                      <button type="submit" class="btn btn-primary" id="submit"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection

@push( 'scripts' )
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

{{-- map --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOrBIdGFN0HlSNMnxUleon8Ql75uBMZd0&libraries=places&callback=initMap" async defer></script>
<script>
{{-- google autocomplete --}}
function initMap() {
  var myLatLng = {lat: {{ $setting->lattitude != '' ? $setting->lattitude:'-6.9031504'}}, lng: {{ $setting->longitude != '' ? $setting->longitude:'107.5919395'}}};
  var map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    zoom: 13
  });

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'sample'
  });

  infowindow.setContent('<div><strong>{{ $setting->address }}</strong>');
  infowindow.open(map, marker);

  var input = document.getElementById('address-autocomplete');

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    $('.lattitude').val(place.geometry.location.lat()).change();
    $('.longitude').val(place.geometry.location.lng()).change();

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });
}
</script>

<script type="text/javascript">
        $(document).keypress(function(e) {
          if(e.which == 13) {
              e.preventDefault();
          }
        });

    $(document).ready( function() {
        $('.summernote').summernote({
          height: 300,
        });
        $("#summernote-tagline .note-codable").attr( "name", "tagline" );
        $("#summernote-keywords .note-codable").attr( "name", "keywords" );
        $(".btn-file").attr("class", "btn btn-info btn-file");

        var child = $( '#parent' ).val();

        if (child != 1) {
            $(".hide_image").hide();
        }

        $('#parent').change(function (){
            var parent = $(this).val();
            console.log( parent );
            if (parent == '1') {
                $(".hide_image").show();
            } else {

                $(".hide_image").hide();
            }

        });
    });

    $( '#submit' ).click( function(e) {
        // e.preventDefault();
        $('#summernote-tagline .note-codable').html($('#summernote-tagline .note-editable').html());
        $('#summernote-keywords .note-codable').html($('#summernote-keywords .note-editable').html());
        // console.log($(this).serialize());

    });
</script>
@endpush
