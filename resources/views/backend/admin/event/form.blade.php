<div class="form-body">
  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Event Type</label>
    <div class="col-md-9">
      {!! Form::select( 'event_type_id', $eventTypes, null, [ 'class' => 'form-control country' , 'required' => true] ) !!}
    	{!! Form::errorMsg('event_type_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Country</label>
    <div class="col-md-9">
      @if($event)
        {!! Form::select( 'country_id', $countries, null, [ 'class' => 'form-control country' , 'required' => true] ) !!}
      @else
        {!! Form::select( 'country_id', ['' => '-- Select Country --'] + $countries, null, [ 'class' => 'form-control country' , 'required' => true] ) !!}
      @endif

    	{!! Form::errorMsg('country_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">Region</label>
    <div class="col-md-9">
        @if($event)
          {!! Form::select( 'region_id', $regions, null, [ 'class' => 'form-control region' , 'required' => true] ) !!}
        @else
          <select name="region_id" class="form-control region" required>
            <option value="">-- Select Region --</option>
          </select>
        @endif
    	{!! Form::errorMsg('region_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="parent">City</label>
    <div class="col-md-9">
        @if($event)
          {!! Form::select( 'city_id', $cities, null, [ 'class' => 'form-control region' , 'required' => true] ) !!}
        @else
          <select name="city_id" class="form-control city" required>
            <option value="">-- Select City --</option>
          </select>
        @endif
    	{!! Form::errorMsg('city_id') !!}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Name</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'name', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('name') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Description</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::textarea( 'description', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('description') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Highlight</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'highlight', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('highlight') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Start Date</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::date( 'start_date', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true,'type=date' => "yyyy-mm-dd"] ) !!}
        {!! Form::errorMsg('start_date') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">End Date</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::date( 'end_date', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true,'type=date' => "yyyy-mm-dd"] ) !!}
        {!! Form::errorMsg('end_date') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Location</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'location', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('location') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Cost</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::number( 'cost', null, [ 'class' => 'form-control empty', 'autocomplete' => 'off', 'required' => true ] ) !!}
        {!! Form::errorMsg('cost') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Image
      <span class="badge badge badge-pill badge-info" data-toggle="popover" data-content="Dummy Size 1204x860"
      data-trigger="hover" data-original-title="Size" data-placement="right">Size
      </span>
    </label>

    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::file( 'image', [ 'class' => 'form-control empty' ] ) !!}
        {!! Form::errorMsg('highlight') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Longitude</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'longitude', null, [ 'class' => 'form-control longitude empty', 'autocomplete' => 'off', 'required' => false ] ) !!}
        {!! Form::errorMsg('longitude') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Latitude</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'lattitude', null, [ 'class' => 'form-control lattitude empty', 'autocomplete' => 'off', 'required' => false ] ) !!}
        {!! Form::errorMsg('lattitude') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Status</label>
    <div class="col-md-9">
      <div class="position-relative">
        {!! Form::text( 'status', null, [ 'class' => 'form-control status empty', 'autocomplete' => 'off', 'required' => false ] ) !!}
        {!! Form::errorMsg('status') !!}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-md-3 label-control" for="timesheetinput1">Is Published</label>
    <div class="col-md-9">
      <fieldset>
        <label class="custom-control custom-checkbox">
          {!! Form::checkbox( 'is_published', (!empty( $event ) ? $event->is_published : 0), null,['class' => 'custom-control-input']) !!}
          <span class="custom-control-indicator"></span>
        </label>
      </fieldset>
    </div>
  </div>
</div>

<div class="form-actions right">
  <a href="{{ route('admin.event.index') }}"><button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> {{ trans('general.back') }}</button></a>
  <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> {{ trans('general.submit') }}</button>
</div>

@push( 'scripts' )
<script type="text/javascript">
  $( document ).ready(function() {
    $(document).on('change','.country',function(){
      var _val = $(this).val();
      var _url = '{!! route("admin.event.ajax") !!}';
      if (_val != '') {
        $.get(_url,{type:'getRegion',country_id:_val},function(result){
          if (Object.keys(result).length > 0) {
            var html = '<option value="">-- Select Region --</option>';
            $.each(result,function(key,value){
              html += '<option value="'+key+'">'+value+'</option>';
            });
            $('.region').html(html);
          }
        });
      }
    });

    $(document).on('change','.region',function(){
      var _val = $(this).val();
      var _url = '{!! route("admin.event.ajax") !!}';
      if (_val != '') {
        $.get(_url,{type:'getCity',region_id:_val},function(result){
          if (Object.keys(result).length > 0) {
            var html = '<option value="">-- Select City --</option>';
            $.each(result,function(key,value){
              html += '<option value="'+key+'">'+value+'</option>';
            });
            $('.city').html(html);
          }
        });
      }
    });
  });
</script>
@endpush
