<script type="text/javascript">
    function editData(id) {
        $('.page-loader').fadeIn();
        var urlAction = $('.btn-edit-'+id).attr('data-href');
        $.ajax({
            type: 'GET',
            url: urlAction,
            success:function(responses, status){
                $('.page-loader').fadeOut();
                if(responses.error === false) {
                    $('#id').val(responses.city.id);
                    $('#name').val(responses.city.name);
                    if(responses.city.is_active) {
                        $('#is_active').prop('checked', true);
                    } else {
                        $('#is_active').prop('checked', false);
                    }
                    $('#modalCity').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                } else {
                    toastr.error(responses.message, responses.title);
                }
            }
        });
        return false;
    }
    $(document).ready(function() {
        $('#country_id').change(function(){
            $('.page-loader').fadeIn();
            var country_id = $(this).val();
            var urlAction = '{{route('master.region.get.province.by.country')}}';
            $.ajax({
                type: 'POST',
                url: urlAction,
                    data: {country_id: country_id},
                success:function(responses, status){
                    $('.page-loader').fadeOut();
                    if(responses.error === false) {
                        resetSelectBox('province_id');
                        appendSelectBox(responses.provinces, 'province_id');
                    } else {
                        toastr.error(responses.message, responses.title);
                    }
                }
            });
            return false;
        });
        var data = $( '#cityTable' ).DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route( 'master.region.city.table' ) !!}",
            columns: [
              { data: 'name', name: 'name' },
              { data: 'province', name: 'province', searchable: false, orderable: false  },
              { data: 'country', name: 'country', searchable: false, orderable: false  },
              { data: 'is_active', name: 'is_active', searchable: false, orderable: false  },
              { data: 'action', name: 'action', class: 'text-center', searchable: false, orderable: false }
            ]
          });
        new $.fn.dataTable.Responsive(data);

        $('.add-button').click(function(){
            $('#formCity')[0].reset();
            $('#modalCity').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#formCity').submit(function(e){
            e.preventDefault();
            $('.page-loader').fadeIn();
            var urlAction = $(this).attr('action');
            var formData = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: urlAction,
                data: formData,
                success:function(responses, status){
                    $('.page-loader').fadeOut();
                    if(responses.error === false) {
                        $('#formCity')[0].reset();
                        $('#modalCity').modal('hide');
                        toastr.info(responses.message, responses.title);
                        window.setTimeout(function() {
                            location.href = responses.redirect;
                        }, 3000);
                    } else {
                        toastr.error(responses.message, responses.title);
                    }
                },
                error: function(responses) {
                    $('.page-loader').fadeOut();
                    var data = $.parseJSON(responses.responseText);
                    $.each(data.errors, function (key, value) {
                        toastr.warning(value, 'Warning!');
                        $('#'+key).addClass('warning-input');
                    });
                }
            });
            return false;
        });
    });
</script>