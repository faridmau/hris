<script type="text/javascript">
    function editData(id) {
        $('.page-loader').fadeIn();
        var urlAction = $('.btn-edit-'+id).attr('data-href');
        $.ajax({
            type: 'GET',
            url: urlAction,
            success:function(responses, status){
                $('.page-loader').fadeOut();
                if(responses.error === false) {
                    $('#id').val(responses.country.id);
                    $('#name').val(responses.country.name);
                    if(responses.country.is_active) {
                        $('#is_active').prop('checked', true);
                    } else {
                        $('#is_active').prop('checked', false);
                    }
                    $('#modalCountry').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                } else {
                    toastr.error(responses.message, responses.title);
                }
            }
        });
        return false;
    }
    $(document).ready(function() {
        var data = $( '#countryTable' ).DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route( 'master.region.country.table' ) !!}",
            columns: [
              { data: 'name', name: 'name' },
              { data: 'is_active', name: 'is_active' },
              { data: 'action', name: 'action', class: 'text-center', searchable: false, orderable: false }
            ]
          });
        new $.fn.dataTable.Responsive(data);

        $('.add-button').click(function(){
            $('#formCountry')[0].reset();
            $('#modalCountry').modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#formCountry').submit(function(e){
            e.preventDefault();
            $('.page-loader').fadeIn();
            var urlAction = $(this).attr('action');
            var formData = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: urlAction,
                data: formData,
                success:function(responses, status){
                    $('.page-loader').fadeOut();
                    if(responses.error === false) {
                        $('#formCountry')[0].reset();
                        $('#modalCountry').modal('hide');
                        toastr.info(responses.message, responses.title);
                        window.setTimeout(function() {
                            location.href = responses.redirect;
                        }, 3000);
                    } else {
                        toastr.error(responses.message, responses.title);
                    }
                },
                error: function(responses) {
                    $('.page-loader').fadeOut();
                    var data = $.parseJSON(responses.responseText);
                    $.each(data.errors, function (key, value) {
                        toastr.warning(value, 'Warning!');
                        $('#'+key).addClass('warning-input');
                    });
                }
            });
            return false;
        });
    });
</script>