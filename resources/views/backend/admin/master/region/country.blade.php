@extends('backend.layouts.admin.master')
@section('title')
Country
@endsection
@push('css_plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/vendors/css/extensions/toastr.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/plugins/extensions/toastr.css">
<!-- END VENDOR CSS-->
@endpush
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                @include('backend.admin.partials.breadcrumb')
                <h3 class="content-header-title mb-0">Country</h3>
            </div>
        </div>
        <div class="content-body">
            @include( 'flash::message' )
            <section id="constructor">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <ul class="nav nav-tabs nav-top-border no-hover-bg">
                                        <li class="nav-item">
                                            <a class="nav-link active"
                                                href="{{ route('master.region.country') }}" aria-expanded="true"> Country </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="baseIcon-tab12" href="{{ route('master.region.province') }}" aria-expanded="false"> Province </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="baseIcon-tab13"
                                                href="{{ route('master.region.city') }}" aria-expanded="false"> City </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content px-1 pt-1">
                                        <div role="tabpanel" class="tab-pane active" id="tabIcon11" aria-expanded="true" aria-labelledby="baseIcon-tab11">
                                            <div class="row content-header">
                                                <div class="col-md-8">
                                                </div>
                                                <div class="col-md-4 content-header-right">
                                                    <div class="form-group float-md-right">
                                                        <button type="button" class="btn btn-float btn-square btn-primary add-button"><i class="fa fa-plus"></i> <span> Add</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <table class="table table-striped table-bordered" id="countryTable">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Is Active</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>
</div>
<div class="modal fade text-left" id="modalCountry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Country Form</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ route('master.region.country.post') }}" method="POST" id="formCountry" class="form-submit">
                <div class="modal-body">
                    <label>Name <sup>*</sup></label>
                    <div class="form-group">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id">
                        <input type="text" name="name" id="name" placeholder="Type country name here" class="form-control">
                    </div>
                    <fieldset>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="is_active" id="is_active" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Is Active </span>
                        </label>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal" type="button"><i class="fa fa-times"></i> Close </button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"> </i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/extensions/toastr.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
@include('backend.admin.master.region.js_country')
@include('backend.admin.forms.delete-modal')
@endpush