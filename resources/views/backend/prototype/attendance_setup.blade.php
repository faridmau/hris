@extends('backend.layouts.admin.master')

@section('title')
Attendance Setup
@endsection

@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<!-- END VENDOR CSS-->
@endsection

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        @include('backend.admin.partials.breadcrumb')
        <h3 class="content-header-title mb-0">Attendance Setup</h3>
      </div>
    </div>
    <div class="content-body">
      @include( 'flash::message' )
      <section id="constructor">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card mb-1">
                                    <div class="card-content">
                                      <div class="bg-primary bg-lighten-1 height-50"></div>
                                      <div class="p-1">
                                        <p class="mb-0">
                                          <strong>09.00</strong>
                                        </p>
                                        <p class="mb-0">current start time</p>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card mb-1">
                                    <div class="card-content">
                                      <div class="bg-primary bg-lighten-1 height-50"></div>
                                      <div class="p-1">
                                        <p class="mb-0">
                                          <strong>17.00</strong>
                                        </p>
                                        <p class="mb-0">current end time</p>
                                      </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group mr-1 mb-1">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(54px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                      <a class="dropdown-item update-action" href="#">Update</a>
                                      <div class="dropdown-divider"></div>
                                      <a class="dropdown-item cancel-action" href="#">Cancel</a>
                                      <div class="dropdown-divider"></div>
                                      <a class="dropdown-item" href="#">Dashboard</a>
                                    </div>
                                  </div>
                            </div>
                        </div>
                        <form action="#" class="hidden-form" style="display:none;">
                          <div class="row">
                            <div class="col-md-6 col-sm-6 col-12">
                              <div class="form-group">
                                <label>Start Time</label>
                                <div class="input-group">
                                  <span class="input-group-addon">
                                    <span class="ft-clock"></span>
                                  </span>
                                  <input type='text' class="form-control pickatime" placeholder="Basic Pick-a-time"
                                  />
                                </div>
                                <small class="text-muted">Use <code>start time</code> to set work hour.
                                </small>
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-12">
                              <div class="form-group">
                                <label>End Time</label>
                                <div class="input-group">
                                  <span class="input-group-addon">
                                    <span class="ft-clock"></span>
                                  </span>
                                  <input type='text' class="form-control pickatime" placeholder="Basic Pick-a-time"
                                  />
                                </div>
                                <small class="text-muted">Use <code>end time</code> to set work hour.
                                </small>
                              </div>
                            </div>
                  </div>
                </form>
              </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
@endsection

@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<script type="text/javascript">
    $(document).ready(function(){
        $('.update-action').click(function(){
            $('.hidden-form').fadeIn();
        });
        $('.cancel-action').click(function(){
            $('.hidden-form').fadeOut();
        });
    })
</script>

@endpush
