@extends('backend.layouts.admin.master')
@section('title')
Job Detail Setup
@endsection
@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<!-- END VENDOR CSS-->
@endsection
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                @include('backend.admin.partials.breadcrumb')
                <h3 class="content-header-title mb-0">Job Detail Setup</h3>
            </div>
        </div>
        <div class="content-body">
            @include( 'flash::message' )
            <section id="constructor">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="card-body">
                                        <div class="col-xl-12 col-lg-12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <ul class="nav nav-tabs nav-linetriangle no-hover-bg">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="base-tab41" data-toggle="tab" aria-controls="tab41" href="#tab41" aria-expanded="true">Job Title</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab42" data-toggle="tab" aria-controls="tab42" href="#tab42" aria-expanded="false">Pay Grades</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab43" data-toggle="tab" aria-controls="tab43" href="#tab43" aria-expanded="false">Employment Status</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content px-1 pt-1">
                                                            <div role="tabpanel" class="tab-pane active" id="tab41" aria-expanded="true" aria-labelledby="base-tab41">
                                                                <div class="card-content collapse show">
                                                                    <div class="row">
                                                                        <div class="form-actions btn-group float-md-right">
                                                                            <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body card-dashboard">
                                                                        <table class="table table-striped table-bordered zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Job Title</th>
                                                                                    <th>Name</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Tiger Nixon</td>
                                                                                    <td>System Architect</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Garrett Winters</td>
                                                                                    <td>Accountant</td>
                                                                                </tr>   
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab42" aria-labelledby="base-tab42">
                                                                <div class="card-content collapse show">
                                                                    <div class="row">
                                                                        <div class="form-actions btn-group float-md-right">
                                                                            <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body card-dashboard">
                                                                        <table class="table table-striped table-bordered zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Min Sallary</th>
                                                                                    <th>Max Salary</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Supervisor</td>
                                                                                    <td>U$ 5000</td>
                                                                                    <td>U$ 8000</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Manager</td>
                                                                                    <td>U$ 5000</td>
                                                                                    <td>U$ 8000</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Staff</td>
                                                                                    <td>U$ 5000</td>
                                                                                    <td>U$ 8000</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Administration</td>
                                                                                    <td>U$ 5000</td>
                                                                                    <td>U$ 8000</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab43" aria-labelledby="base-tab43">
                                                                <div class="card-content collapse show">
                                                                    <div class="row">
                                                                        <div class="form-actions btn-group float-md-right">
                                                                            <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body card-dashboard">
                                                                        <table class="table table-striped table-bordered zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Description</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Full Time</td>
                                                                                    <td>Full time work</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Freelance</td>
                                                                                    <td>Freelance work</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Intern</td>
                                                                                    <td>Intern work</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>
</div>
@endsection
@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<script type="text/javascript">
    $(document).ready(function(){
        $('.update-action').click(function(){
            $('.hidden-form').fadeIn();
        });
        $('.cancel-action').click(function(){
            $('.hidden-form').fadeOut();
        });
    })
</script>
@endpush