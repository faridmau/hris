@extends('backend.layouts.admin.master')
@section('title')
Company Loans
@endsection
@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<!-- END VENDOR CSS-->
@endsection
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                @include('backend.admin.partials.breadcrumb')
                <h3 class="content-header-title mb-0">Company Loans</h3>
            </div>
        </div>
        <div class="content-body">
            @include( 'flash::message' )
            <section id="constructor">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs nav-top-border no-hover-bg">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="baseIcon-tab11" data-toggle="tab" aria-controls="tabIcon11"
                                                    href="#tabIcon11" aria-expanded="true"><i class="fa fa-play"></i> Tab 1</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="baseIcon-tab12" data-toggle="tab" aria-controls="tabIcon12"
                                                    href="#tabIcon12" aria-expanded="false"><i class="fa fa-flag"></i> Tab 2</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="baseIcon-tab13" data-toggle="tab" aria-controls="tabIcon13"
                                                    href="#tabIcon13" aria-expanded="false"><i class="fa fa-cog"></i> Tab 3</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link disabled"><i class="fa fa-unlink"></i> Disabled</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content px-1 pt-1">
                                            <div role="tabpanel" class="tab-pane active" id="tabIcon11" aria-expanded="true"
                                                aria-labelledby="baseIcon-tab11">
                                                <p>Oat cake marzipan cake lollipop caramels wafer pie jelly
                                                    beans. Icing halvah chocolate cake carrot cake. Jelly beans
                                                    carrot cake marshmallow gingerbread chocolate cake. Gummies
                                                    cupcake croissant.
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tabIcon12" aria-labelledby="baseIcon-tab12">
                                                <p>Sugar plum tootsie roll biscuit caramels. Liquorice brownie
                                                    pastry cotton candy oat cake fruitcake jelly chupa chups.
                                                    Pudding caramels pastry powder cake soufflé wafer caramels.
                                                    Jelly-o pie cupcake.
                                                </p>
                                            </div>
                                            <div class="tab-pane" id="tabIcon13" aria-labelledby="baseIcon-tab3">
                                                <p>Biscuit ice cream halvah candy canes bear claw ice cream
                                                    cake chocolate bar donut. Toffee cotton candy liquorice.
                                                    Oat cake lemon drops gingerbread dessert caramels. Sweet
                                                    dessert jujubes powder sweet sesame snaps.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>
</div>
@endsection
@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<script type="text/javascript">
    $(document).ready(function(){
        $('.update-action').click(function(){
            $('.hidden-form').fadeIn();
        });
        $('.cancel-action').click(function(){
            $('.hidden-form').fadeOut();
        });
    })
</script>
@endpush