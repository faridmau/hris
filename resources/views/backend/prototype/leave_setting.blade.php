@extends('backend.layouts.admin.master')
@section('title')
Leave Setting
@endsection
@section('plugin_page')
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/responsive.dataTables.min.css">
<!-- END VENDOR CSS-->
@endsection
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                @include('backend.admin.partials.breadcrumb')
                <h3 class="content-header-title mb-0">Leave Setting</h3>
            </div>
        </div>
        <div class="content-body">
            @include( 'flash::message' )
            <section id="constructor">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="card-body">
                                        <div class="col-xl-12 col-lg-12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <ul class="nav nav-tabs nav-linetriangle no-hover-bg">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" id="base-tab41" data-toggle="tab" aria-controls="tab41" href="#tab41" aria-expanded="true">Leave Types</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab42" data-toggle="tab" aria-controls="tab42" href="#tab42" aria-expanded="false">Leave Period</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab43" data-toggle="tab" aria-controls="tab43" href="#tab43" aria-expanded="false">Work Week</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab44" data-toggle="tab" aria-controls="tab44" href="#tab44" aria-expanded="false">Holiday</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab45" data-toggle="tab" aria-controls="tab45" href="#tab45" aria-expanded="false">Leave Rules</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" id="base-tab46" data-toggle="tab" aria-controls="tab46" href="#tab46" aria-expanded="false">Employee Leave List</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content px-1 pt-1">
                                                            <div role="tabpanel" class="tab-pane active" id="tab41" aria-expanded="true" aria-labelledby="base-tab41">
                                                                <div class="card-content collapse show">
                                                                    <div class="row">
                                                                        <div class="form-actions btn-group float-md-right">
                                                                            <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body card-dashboard">
                                                                        <table class="table table-striped table-bordered zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Description</th>
                                                                                    <th>Status</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Annual Leave</td>
                                                                                    <td>Annual Leave</td>
                                                                                    <td>Active</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Casual Leave</td>
                                                                                    <td>Casual Leave</td>
                                                                                    <td>Active</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Medical Leave</td>
                                                                                    <td>Medical Leave</td>
                                                                                    <td>Active</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab42" aria-labelledby="base-tab42">
                                                                <div class="card-content collapse show">
                                                                    <div class="row">
                                                                        <div class="form-actions btn-group float-md-right">
                                                                            <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body card-dashboard">
                                                                        <table class="table table-striped table-bordered zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Peride Start</th>
                                                                                    <th>Peride End</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Year 2018</td>
                                                                                    <td>1 January 2018</td>
                                                                                    <td>31 December 2017</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Year 2017</td>
                                                                                    <td>1 January 2017</td>
                                                                                    <td>31 December 2017</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Year 2016</td>
                                                                                    <td>1 January 2016</td>
                                                                                    <td>31 December 2017</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab43" aria-labelledby="base-tab43">
                                                                <div class="card-content collapse show">
                                                                    <div class="row">
                                                                        <div class="form-actions btn-group float-md-right">
                                                                            <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card-body card-dashboard">
                                                                        <table class="table table-striped table-bordered zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Day</th>
                                                                                    <th>Status</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Monday</td>
                                                                                    <td>Full Day</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Tuesday</td>
                                                                                    <td>Full Day</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Wednesday</td>
                                                                                    <td>Full Day</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Thursday</td>
                                                                                    <td>Full Day</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Friday</td>
                                                                                    <td>Full Day</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Saturday</td>
                                                                                    <td>Half Day</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Sunday</td>
                                                                                    <td>Non Working Day</td>
                                                                                </tr>   
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab44" aria-labelledby="base-tab44">
                                                                        <div class="card-content collapse show">
                                                                            <div class="row">
                                                                                <div class="form-actions btn-group float-md-right">
                                                                                    <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="card-body card-dashboard">
                                                                                <table class="table table-striped table-bordered zero-configuration">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Name</th>
                                                                                            <th>Date</th>
                                                                                            <th>Description</th>
                                                                                            <th>Status</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>New Year 2018</td>
                                                                                            <td>1 January 2018</td>
                                                                                            <td>Happy New Year</td>
                                                                                            <td>Full Day</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Tahun baru Implek</td>
                                                                                            <td>16 Februari</td>
                                                                                            <td>Tahun Baru Implek</td>
                                                                                            <td>Full Day</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="tab45" aria-labelledby="base-tab45">
                                                                        <div class="card-content collapse show">
                                                                            <div class="row">
                                                                                <div class="form-actions btn-group float-md-right">
                                                                                    <a href="" class="btn btn-secondary"><i class="ft-plus"></i>Add New</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="card-body card-dashboard">
                                                                                <table class="table table-striped table-bordered zero-configuration">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Namesss</th>
                                                                                            <th>Date</th>
                                                                                            <th>Description</th>
                                                                                            <th>Status</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>New Year 2018</td>
                                                                                            <td>1 January 2018</td>
                                                                                            <td>Happy New Year</td>
                                                                                            <td>Full Day</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Tahun baru Implek</td>
                                                                                            <td>16 Februari</td>
                                                                                            <td>Tahun Baru Implek</td>
                                                                                            <td>Full Day</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>
</div>
@endsection
@push( 'scripts' )
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset( 'assets/backend')}}/js/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<script type="text/javascript">
    $(document).ready(function(){
        $('.update-action').click(function(){
            $('.hidden-form').fadeIn();
        });
        $('.cancel-action').click(function(){
            $('.hidden-form').fadeOut();
        });
    })
</script>
@endpush