
@extends('backend.layouts.auth.master')

@section('title')
Login Page
@endsection

@push('css_plugin_page')
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/pages/timeline.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/plugins/forms/wizard.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/plugins/pickers/daterange/daterange.css">
@endpush

@section('content')
<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0">Form Signup</h3>
        <div class="row breadcrumbs-top">
          <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a>
              </li>
              <li class="breadcrumb-item"><a href="#">Signup</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
      <div class="content-header-right col-md-6 col-12">
      </div>
    </div>
    <div class="content-body">
      <!-- Form wizard with icon tabs section start -->
      <section id="icon-tabs">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <form action="#" class="icons-tab-steps wizard-circle">
                    <!-- Step 1 -->
                    <h6><i class="step-icon fa fa-user"></i> General Profile</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="firstName2">First Name :</label>
                            <input type="text" class="form-control" id="firstName2">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="lastName2">Last Name :</label>
                            <input type="text" class="form-control" id="lastName2">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="emailAddress3">Email Address :</label>
                            <input type="email" class="form-control" id="emailAddress3">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="location2">Select City :</label>
                            <select class="custom-select form-control" id="location2" name="location">
                              <option value="">Select City</option>
                              <option value="Amsterdam">Amsterdam</option>
                              <option value="Berlin">Berlin</option>
                              <option value="Frankfurt">Frankfurt</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="phoneNumber2">Phone Number :</label>
                            <input type="tel" class="form-control" id="phoneNumber2">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="date2">Date of Birth :</label>
                            <input type="date" class="form-control pickadate" id="date2">
                          </div>
                        </div>
                      </div>
                    </fieldset>
                    <!-- Step 2 -->
                    <h6><i class="step-icon fa fa-building"></i>Division</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="proposalTitle2">Proposal Title :</label>
                            <input type="text" class="form-control" id="proposalTitle2">
                          </div>
                          <div class="form-group">
                            <label for="emailAddress4">Email Address :</label>
                            <input type="email" class="form-control" id="emailAddress4">
                          </div>
                          <div class="form-group">
                            <label for="videoUrl2">Video URL :</label>
                            <input type="url" class="form-control" id="videoUrl2">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="jobTitle3">Job Title :</label>
                            <input type="text" class="form-control" id="jobTitle3">
                          </div>
                          <div class="form-group">
                            <label for="shortDescription2">Short Description :</label>
                            <textarea name="shortDescription" id="shortDescription2" rows="4" class="form-control"></textarea>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                    <!-- Step 3 -->
                    <h6><i class="step-icon fa fa-suitcase"></i>Experiences</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="eventName2">Event Name :</label>
                            <input type="text" class="form-control" id="eventName2">
                          </div>
                          <div class="form-group">
                            <label for="eventType2">Event Type :</label>
                            <select class="custom-select form-control" id="eventType2" data-placeholder="Type to search cities"
                            name="eventType2">
                              <option value="Banquet">Banquet</option>
                              <option value="Fund Raiser">Fund Raiser</option>
                              <option value="Dinner Party">Dinner Party</option>
                              <option value="Wedding">Wedding</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="eventLocation2">Event Location :</label>
                            <select class="custom-select form-control" id="eventLocation2" name="location">
                              <option value="">Select City</option>
                              <option value="Amsterdam">Amsterdam</option>
                              <option value="Berlin">Berlin</option>
                              <option value="Frankfurt">Frankfurt</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Event Date - Time :</label>
                            <div class='input-group'>
                              <input type='text' class="form-control datetime" />
                              <span class="input-group-addon">
                                <span class="fa fa-calendar-o"></span>
                              </span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="eventStatus2">Event Status :</label>
                            <select class="custom-select form-control" id="eventStatus2" name="eventStatus">
                              <option value="Planning">Planning</option>
                              <option value="In Progress">In Progress</option>
                              <option value="Finished">Finished</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Requirements :</label>
                            <div class="c-inputs-stacked">
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">Staffing</span>
                              </label>
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">Catering</span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                    <!-- Step 4 -->
                    <h6><i class="step-icon fa fa-black-tie"></i>Portofolio</h6>
                    <fieldset>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="meetingName2">Name of Meeting :</label>
                            <input type="text" class="form-control" id="meetingName2">
                          </div>
                          <div class="form-group">
                            <label for="meetingLocation2">Location :</label>
                            <input type="text" class="form-control" id="meetingLocation2">
                          </div>
                          <div class="form-group">
                            <label for="participants2">Names of Participants</label>
                            <textarea name="participants" id="participants2" rows="4" class="form-control"></textarea>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="decisions2">Decisions Reached</label>
                            <textarea name="decisions" id="decisions2" rows="4" class="form-control"></textarea>
                          </div>
                          <div class="form-group">
                            <label>Agenda Items :</label>
                            <div class="c-inputs-stacked">
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">1st item</span>
                              </label>
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">2nd item</span>
                              </label>
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">3rd item</span>
                              </label>
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">4th item</span>
                              </label>
                              <label class="inline custom-control custom-checkbox block">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description ml-0">5th item</span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Form wizard with icon tabs section end -->
    </div>
  </div>
</div>
@endsection
@push('script_pages')

@endpush
