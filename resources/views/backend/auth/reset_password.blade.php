@extends('backend.layouts.auth.master')
@section('title')
Reset Password
@endsection
@section('plugin-page')
@endsection
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title mb-0">Welcome to HRIS</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">HRIS</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Login</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
            </div>
        </div>
        <div class="content-body">
            <div class="col-md-12 basic-form-layouts">
                <div class="card">
                    <div class="card-header">
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="card-text">
                                {!! Form::open( [ 'route' => 'auth.update.password' ,'class' => 'form'] ) !!}
                                <input type="hidden" name="id" value="{{ $id }}">
                                <input type="hidden" name="code" value="{{ $code }}">
                                <div class="row justify-content-md-center">
                                    <div class="col-md-6">
                                        <div class="form-body">
                                            <div class="alert alert-primary border-0 mb-2" role="alert">
                                                <strong>Tips!</strong> Use combination character to create strong password!
                                            </div>
                                            <div class="form-group">
                                                <label for="eventInput1">New Password</label>
                                                <input type="password" id="eventInput1" class="form-control" placeholder="type new password " name="password">
                                            </div>
                                            <div class="form-group">
                                                <label for="eventInput1">Password Confirmation</label>
                                                <input type="password" id="eventInput1" class="form-control" placeholder="type password confirmation " name="password_confirmation">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-actions center justify-content-md-center">
                                                        <a class="btn btn-default btn-block" href="{{route('auth.login')}}">
                                                        <i class="fa fa-sign-in"></i> Back to Login
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-actions center justify-content-md-center">
                                                        <button type="submit" class="btn btn-primary btn-block">
                                                        <i class="fa fa-sign-in"></i> Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push( 'scripts' )
@endpush