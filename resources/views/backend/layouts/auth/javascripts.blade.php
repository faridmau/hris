      <!-- BEGIN VENDOR JS-->
      <script src="{{ asset( 'assets/backend')}}/js/vendors.min.js" type="text/javascript"></script>
      <!-- BEGIN VENDOR JS-->
      <!-- BEGIN PAGE VENDOR JS-->
      <script type="text/javascript" src="{{ asset( 'assets/backend')}}/js/vendors/ui/jquery.sticky.js"></script>
      <script src="{{ asset( 'assets/backend')}}/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
      <script src="{{ asset( 'assets/backend')}}/vendors/js/extensions/jquery.steps.min.js" type="text/javascript"></script>
      <!-- END PAGE VENDOR JS-->
      <!-- BEGIN STACK JS-->
      <script src="{{ asset( 'assets/backend')}}/js/app-menu.js" type="text/javascript"></script>
      <script src="{{ asset( 'assets/backend')}}/js/app.js" type="text/javascript"></script>

      <!-- END STACK JS-->
      <!-- BEGIN PAGE LEVEL JS-->
      <script src="{{ asset( 'assets/backend')}}/js/forms/wizard-steps.min.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL JS-->
       @stack( 'script_pages' )