<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
    <head>
        @include('backend.layouts.auth.meta')
        <title>@yield('title') | {{ env('APP_NAME') }}</title>
        @include('backend.layouts.auth.css')
    </head>
    <body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns   menu-expanded">
        @include('backend.layouts.auth.header')
        {{-- content --}}
        @yield('content')
        {{-- end content --}}
        @include('backend.layouts.auth.footer')
        @include('backend.layouts.auth.javascripts')
</body>
</html>