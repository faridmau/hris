<!-- Horizontal navigation-->
  <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
    <!-- Horizontal menu content-->
    <div data-menu="menu-container" class="navbar-container main-menu-content">
      <!-- include ../../../includes/mixins-->
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
        <li data-menu="" class="nav-item">
          <a href="{{route('auth.login')}}" data-toggle="" class="nav-link"><i class="fa fa-sign-in"></i>
            <span>Login</span>
          </a>
        </li>
        <li data-menu="" class="nav-item"><a href="{{route('auth.signup')}}" data-toggle="" class="nav-link"><i class="fa fa-user-circle-o"></i><span>Signup</span></a>
        </li>
        <li data-menu="" class="nav-item"><a href="{{route('auth.help')}}" data-toggle="" class="nav-link"><i class="fa fa-question-circle"></i><span>Help</span></a>
        </li>
      </ul>
    </div>
    <!-- /horizontal menu content-->
</div>
