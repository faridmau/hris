<!-- Horizontal navigation-->
  <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
    <!-- Horizontal menu content-->
        <div data-menu="menu-container" class="navbar-container main-menu-content">
            {!! Menu::mainMenu(url('admin/'))!!}
        </div>
    <!-- /horizontal menu content-->
  </div>