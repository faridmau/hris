    <div class="page-loader"></div>
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 content">
          <span class="float-md-left d-block d-md-inline-block">Copyright &copy; {{date('Y')}} <a href="http://futura-labs.com"
            target="_blank" class="text-bold-800 grey darken-2">Futura Labs </a>, All rights
            reserved. </span>
        </p>
    </footer>