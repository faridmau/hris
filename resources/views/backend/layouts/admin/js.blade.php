  <script src="{{ asset( 'assets/backend')}}/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script type="text/javascript" src="{{ asset( 'assets/backend')}}/js/vendors/ui/jquery.sticky.js"></script>
  <script type="text/javascript" src="{{ asset( 'assets/backend')}}/js/vendors/charts/jquery.sparkline.min.js"></script>
  <script src="{{ asset( 'assets/backend')}}/js/vendors/charts/raphael-min.js" type="text/javascript"></script>
  <script src="{{ asset( 'assets/backend')}}/js/vendors/charts/morris.min.js" type="text/javascript"></script>
  <script src="{{ asset( 'assets/backend')}}/js/vendors/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="{{ asset( 'assets/backend')}}/js/vendors/timeline/horizontal-timeline.js" type="text/javascript"></script>
  <script src="{{ asset( 'assets/backend')}}/js/vendors/extensions/fullcalendar.min.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="{{ asset( 'assets/backend')}}/js/app-menu.js" type="text/javascript"></script>
  <script src="{{ asset( 'assets/backend')}}/js/app.js" type="text/javascript"></script>
  <script src="{{ asset( 'assets/backend')}}/js/custom.js" type="text/javascript"></script>

  @stack('scripts')
