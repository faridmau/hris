<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    @include('backend.layouts.admin.meta')
    <title>@yield('title') | {{ env('APP_NAME') }}</title>
    @include('backend.layouts.admin.css')
</head>
    <body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns   menu-expanded">
      <!-- fixed-top-->
        @include('backend.layouts.admin.header')
        @include('backend.layouts.admin.navigation')
        <!-- Content-->
         @yield('content')
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        @include('backend.layouts.admin.footer')

        <!-- BEGIN VENDOR JS-->
        @include('backend.layouts.admin.js')
      <!-- END PAGE LEVEL JS-->
    </body>
</html>