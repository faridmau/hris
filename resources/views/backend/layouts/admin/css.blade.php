    <link rel="apple-touch-icon" href="{{ asset( 'assets/backend')}}/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset( 'assets/backend')}}/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/extensions/unslider.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/weather-icons/climacons.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/fonts/meteocons/style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/vendors/charts/morris.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/app.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/css/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets/backend')}}/fonts/simple-line-icons/style.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    @stack( 'css_plugin_page' )
    <!-- END Custom CSS-->


