<!DOCTYPE HTML>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>

<!-- Define Charset -->
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

<!-- Responsive Meta Tag -->
<meta name='viewport' content='width=device-width; initial-scale=1.0; maximum-scale=1.0;'>

    <title>Salamat datang di HRIS</title><!-- Responsive Styles and Valid Styles -->

    <style type='text/css'>

        body{
            width: 100%;
            background-color: #4c4e4e;
            margin:0;
            padding:0;
            -webkit-font-smoothing: antialiased;
        }

        html{
            width: 100%;
        }

        table{
            font-size: 14px;
            border: 0;
        }

        /* ----------- responsivity ----------- */
        @media only screen and (max-width: 640px){

            /*------ top header ------ */
            .header-bg{width: 440px !important; height: 10px !important;}
            .main-header{line-height: 28px !important;}
            .main-subheader{line-height: 28px !important;}

            .container{width: 440px !important;}
            .container-middle{width: 420px !important;}
            .mainContent{width: 400px !important;}

            .main-image{width: 400px !important; height: auto !important;}
            .banner{width: 400px !important; height: auto !important;}
            /*------ sections ---------*/
            .section-item{width: 400px !important;}
            .section-img{width: 400px !important; height: auto !important;}
            /*------- prefooter ------*/
            .prefooter-header{padding: 0 10px !important; line-height: 24px !important;}
            .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;}
            /*------- footer ------*/
            .top-bottom-bg{width: 420px !important; height: auto !important;}

        }

        @media only screen and (max-width: 479px){

            /*------ top header ------ */
            .header-bg{width: 280px !important; height: 10px !important;}
            .top-header-left{width: 260px !important; text-align: center !important;}
            .top-header-right{width: 260px !important;}
            .main-header{line-height: 28px !important; text-align: center !important;}
            .main-subheader{line-height: 28px !important; text-align: center !important;}

            /*------- header ----------*/
            .logo{width: 260px !important;}
            .nav{width: 260px !important;}

            .container{width: 280px !important;}
            .container-middle{width: 260px !important;}
            .mainContent{width: 240px !important;}

            .main-image{width: 240px !important; height: auto !important;}
            .banner{width: 240px !important; height: auto !important;}
            /*------ sections ---------*/
            .section-item{width: 240px !important;}
            .section-img{width: 240px !important; height: auto !important;}
            /*------- prefooter ------*/
            .prefooter-header{padding: 0 10px !important;line-height: 28px !important;}
            .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;}
            /*------- footer ------*/
            .top-bottom-bg{width: 260px !important; height: auto !important;}

        }


    </style>

</head>

<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
    <table border='0' width='100%' cellpadding='0' cellspacing='0'>
        <tr><td height='30'></td></tr>
        <tr bgcolor='#4c4e4e'>
            <td width='100%' align='center' valign='top' bgcolor='#4c4e4e'>

                <!---------   top header   ------------>
                <table border='0' width='600' cellpadding='0' cellspacing='0' align='center' class='container'>
                    <tr>
                        <td><img style='display: block;' src='http://promailthemes.com/campaigner/layout1/white/blue/img/top-header-bg.png' alt='' class='header-bg' /></td>
                    </tr>

                    <tr bgcolor='2780cb'><td height='5'></td></tr>

                    <tr bgcolor='2780cb'>
                        <td align='center'>
                            <table border='0' width='560' align='center' cellpadding='0' cellspacing='0' class='container-middle'>
                                <tr>
                                    <td>
                                        <table border='0' align='left' cellpadding='0' cellspacing='0' class='top-header-left'>
                                            <tr>
                                                <td align='center'>
                                                    <table border='0' cellpadding='0' cellspacing='0' class='date'>
                                                        <tr>
                                                            <td>&nbsp;&nbsp;</td>
                                                            <td mc:edit='date' style='color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;'>
                                                                <singleline>
                                                                    {{$today}}
                                                                </singleline>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <table border='0' align='left' cellpadding='0' cellspacing='0' align='center' class='top-header-right'>
                                            <tr><td width='30' height='20'></td></tr>
                                        </table>

                                        <table border='0' align='right' cellpadding='0' cellspacing='0' align='center' class='top-header-right'>
                                            <tr>
                                                <td align='center'>
                                                    <table border='0' cellpadding='0' cellspacing='0' align='center' class='tel'>
                                                        <tr>
                                                            <td>&nbsp;&nbsp;</td>
                                                            <td mc:edit='tel' style='color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;'>
                                                                <singleline>
                                                                    Phone : +6221-83794811 (Hunting)
                                                                </singleline>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr bgcolor='2780cb'><td height='10'></td></tr>

                </table>

                <!----------    end top header    ------------>


                <!----------   main content----------->
                <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='container' bgcolor='white'>


                    <!--------- Header  ---------->
                    <tr bgcolor='white'><td height='25'></td></tr>

                    <tr>
                        <td>
                            <table border='0' width='560' align='center' cellpadding='0' cellspacing='0' class='container-middle'>
                                <tr>
                                    <td>
                                        <table border='0' align='left' cellpadding='0' cellspacing='0' class='logo'>
                                            <tr>
                                                <td align='center'>
                                                    <a href='' style='display: block;'><img editable='true' mc:edit='logo' width='155' style='display: block;' src='http://via.placeholder.com/155x39' alt='logo' /></a>
                                                </td>
                                            </tr>
                                        </table>
                                        <table border='0' align='left' cellpadding='0' cellspacing='0' class='nav'>
                                            <tr>
                                                <td height='20' width='20'></td>
                                            </tr>
                                        </table>
                                        <table border='0' align='right' cellpadding='0' cellspacing='0' class='nav'>
                                            <tr><td height='10'></td></tr>
                                            <tr>
                                                <td align='center' mc:edit='navigation' style='font-size: 13px; font-family: Helvetica, Arial, sans-serif;'>
                                                    <multiline>

                                                    </multiline>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr bgcolor='white'><td height='25'></td></tr>
                    <!---------- end header --------->


                    <!--------- main section --------->
                    <tr>
                        <td>
                            <table border='0' width='600' align='center' cellpadding='0' cellspacing='0' class='container-middle'>


                                <tr bgcolor='ececec'><td height='80'></td></tr>

                                <tr bgcolor='ececec'>
                                    <td>
                                        <table width='528' border='0' align='center' cellpadding='0' cellspacing='0' class='mainContent'>
                                            <tr>
                                                <td mc:edit='title1' class='main-header' style='color: #484848; font-size: 24px; font-weight: 700; font-family: Helvetica, Arial, sans-serif; text-align:center;'>
                                                    <multiline>
                                                        Forgot your password?
                                                    </multiline>
                                                </td>
                                            </tr>

                                            <tr><td height='20'></td></tr>
                                            <tr>
                                                <td mc:edit='subtitle1' class='main-subheader' style='color: grey; font-size: 19px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;text-align:center;'>
                                                    <multiline>
                                                        Dear {{$name}},
                                                    </multiline>
                                                </td>
                                            </tr>
                                            <tr><td height='20'></td></tr>
                                            <tr>
                                                <td mc:edit='subtitle1' class='main-subheader' style='color: grey; font-size: 14px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;text-align:center;'>
                                                    <multiline>
                                                        Kami telah menerima permintaan anda untuk me-reset password HRIS. Silahkan klik tombol di bawah ini untuk mereset password Anda.
                                                    </multiline>
                                                </td>
                                            </tr>
                                            <tr><td height='40'></td></tr>
                                            <tr>
                                                <td mc:edit='subtitle1' class='main-subheader' style='color: #a4a4a4; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;text-align:center;'>
                                                    {!! link_to_route( 'auth.reset', 'Reset Your Password', [ 'id' => $id, 'code' => $code ], ['style'=>'padding:10px 50px; border-radius:30px; color:white; background:#2780cb;text-decoration:none;','target' => '_blank'] ) !!}
                                                </td>
                                            </tr>
                                            <tr><td height='40'></td></tr>
                                            <tr>
                                                <td mc:edit='subtitle1' class='main-subheader' style='color: grey; font-size: 14px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;text-align:center;'>
                                                    <multiline>
                                                        Abaikan email ini jika anda tidak pernah meminta untuk mereset Password.
                                                    </multiline>
                                                </td>
                                            </tr>
                                            <tr><td height='50'></td></tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr bgcolor='ececec'><td height='25'></td></tr>

                                <tr><td align='center'><img style='display: block;' width='600' height='auto' src='http://promailthemes.com/campaigner/layout1/white/blue/img/bottom-rounded-bg.png' alt='' class='top-bottom-bg' /></td></tr>

                            </table>
                        </td>
                    </tr><!--------- end main section --------->

                    <tr><td height='40'></td></tr>


                    <!---------- prefooter  --------->

                    <tr><td height='30'></td></tr>
                </table>
                <!------------ end main Content ----------------->


                <!---------- footer  --------->
                <table border='0' width='600' cellpadding='0' cellspacing='0' class='container'>
                    <tr bgcolor='2780cb'><td height='14'></td></tr>
                    <tr bgcolor='2780cb'>
                        <td mc:edit='copy3' align='center' style='color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;' height="19">
                            <multiline>
                                <a href='#' target='_blank' style='color: #cecece;text-decoration:none;'>HRIS</a> © Copyright 2017 . All Rights Reserved
                            </multiline>
                        </td>
                    </tr>

                    <tr>
                        <td><img style='display: block; ' src='http://promailthemes.com/campaigner/layout1/white/blue/img/bottom-footer-bg.png' alt='' class='header-bg' /></td>
                    </tr>
                </table>
                <!---------  end footer --------->
            </td>
        </tr>

        <tr><td height='30'></td></tr>

    </table>


</body>
</html>
