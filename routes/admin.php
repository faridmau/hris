<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
|
|
*/
Route::get( '/help' , [ 'as' => 'help.page', 'uses' => 'AuthController@login' ]);
Route::group( [ 'namespace' => 'Auth', 'prefix' => '' ], function() {
    Route::group( [ 'middleware' => 'sentinel_guest' ], function() {
        Route::get( '/' , [ 'as' => 'auth.login', 'uses' => 'AuthController@login' ]);
        Route::get( '/help' , [ 'as' => 'auth.help', 'uses' => 'AuthController@help' ]);
        Route::get( '/signup' , [ 'as' => 'auth.signup', 'uses' => 'AuthController@signup' ]);
        Route::post( 'signin/post' , [ 'as' => 'auth.store', 'uses' => 'AuthController@store' ]);
        Route::get( 'forgot-password', ['as' => 'auth.forgot.password', 'uses' => 'AuthController@forgot' ]);
        Route::post( 'forgot-password/post', ['as' => 'auth.forgot.store', 'uses' => 'AuthController@sendForgot' ]);
        Route::get( 'reset-password/{id}/{code}', ['as' => 'auth.reset', 'uses' => 'AuthController@resetPassword' ]);
        Route::post( 'reset-password/post', ['as' => 'auth.update.password', 'uses' => 'AuthController@updatePassword' ]);
    });
    Route::get( 'signout', ['as' => 'auth.logout', 'uses' => 'AuthController@logout' ]);
});
Route::group( [ 'namespace' => 'Admin', 'prefix' => 'admin','middleware' => 'login_auth' ], function() {
    Route::group( ['prefix' => 'job-setup' ], function() {
        Route::group( ['prefix' => 'job-title' ], function() {
            Route::get( '/', [ 'as' => 'job.setup.job.title', 'uses' => 'JobSetupController@jobTitle' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'job.setup.job.title.delete', 'uses' => 'JobSetupController@destroyJobTitle' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'job.setup.job.title.get.by.id', 'uses' => 'JobSetupController@getJobTitleByID' ] );
            Route::post( 'post', [ 'as' => 'job.setup.job.title.post', 'uses' => 'JobSetupController@postJobTitle' ] );
            Route::get( 'table', [ 'as' => 'admin.job.title.table', 'uses' => 'JobSetupController@jobTitleDataTable' ] );
        });

        Route::group( ['prefix' => 'pay-grade' ], function() {
            Route::get( '/', [ 'as' => 'job.setup.pay.grade', 'uses' => 'JobSetupController@payGrade' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'job.setup.pay.grade.delete', 'uses' => 'JobSetupController@destroyPayGrade' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'job.setup.pay.grade.get.by.id', 'uses' => 'JobSetupController@getPayGradeByID' ] );
            Route::post( 'post', [ 'as' => 'job.setup.pay.grade.post', 'uses' => 'JobSetupController@postPayGrade' ] );
            Route::get( 'table', [ 'as' => 'admin.pay.grade.table', 'uses' => 'JobSetupController@payGradeDataTable' ] );
        });

        Route::group( ['prefix' => 'employment-status' ], function() {
            Route::get( '/', [ 'as' => 'job.setup.employment.status', 'uses' => 'JobSetupController@employmentStatus' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'job.setup.employment.status.delete', 'uses' => 'JobSetupController@destroyEmploymentStatus' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'job.setup.employment.status.get.by.id', 'uses' => 'JobSetupController@getEmploymentStatusByID' ] );
            Route::post( 'post', [ 'as' => 'job.setup.employment.status.post', 'uses' => 'JobSetupController@postEmploymentStatus' ] );
            Route::get( 'table', [ 'as' => 'admin.employment.status.table', 'uses' => 'JobSetupController@employmentStatusDataTable' ] );
        });
    });

    Route::group( ['prefix' => 'company-loan' ], function() {

        Route::group( ['prefix' => 'loan-type' ], function() {
            Route::get( '/', [ 'as' => 'company.loan.loan.type', 'uses' => 'CompanyLoanController@loanType' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'company.loan.loan.type.delete', 'uses' => 'CompanyLoanController@destroyLoanType' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'company.loan.loan.type.get.by.id', 'uses' => 'CompanyLoanController@getLoanTypeByID' ] );
            Route::post( 'post', [ 'as' => 'company.loan.loan.type.post', 'uses' => 'CompanyLoanController@postLoanType' ] );
            Route::get( 'table', [ 'as' => 'admin.loan.type.table', 'uses' => 'CompanyLoanController@loanTypeDataTable' ] );
        });

        Route::group( ['prefix' => 'employee-loan' ], function() {
            Route::get( '/', [ 'as' => 'company.loan.employee.loan', 'uses' => 'CompanyLoanController@employeeLoan' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'company.loan.employee.loan.delete', 'uses' => 'CompanyLoanController@destroyEmployeeLoan' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'company.loan.employee.loan.get.by.id', 'uses' => 'CompanyLoanController@getEmployeeLoanByID' ] );
            Route::post( 'post', [ 'as' => 'company.loan.employee.loan.post', 'uses' => 'CompanyLoanController@postEmployeeLoan' ] );
            Route::get( 'table', [ 'as' => 'admin.employee.loan.table', 'uses' => 'CompanyLoanController@employeeLoanDataTable' ] );
        });

    });


    Route::group( ['prefix' => 'reimbursement' ], function() {

        Route::group( ['prefix' => 'type' ], function() {
            Route::get( '/', [ 'as' => 'reimbursement.administration.reimbursement.type', 'uses' => 'ReimbursementAdministrationController@reimbursementType' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'reimbursement.administration.reimbursement.type.delete', 'uses' => 'ReimbursementAdministrationController@destroyReimbursementType' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'reimbursement.administration.reimbursement.type.get.by.id', 'uses' => 'ReimbursementAdministrationController@getReimbursementTypeByID' ] );
            Route::post( 'post', [ 'as' => 'reimbursement.administration.reimbursement.type.post', 'uses' => 'ReimbursementAdministrationController@postReimbursementType' ] );
            Route::get( 'table', [ 'as' => 'admin.reimbursement.type.table', 'uses' => 'ReimbursementAdministrationController@reimbursementTypeDataTable' ] );
        });

        Route::group( ['prefix' => 'payment-method' ], function() {
            Route::get( '/', [ 'as' => 'reimbursement.administration.payment.method', 'uses' => 'ReimbursementAdministrationController@paymentMethod' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'reimbursement.administration.payment.method.delete', 'uses' => 'ReimbursementAdministrationController@destroyPaymentMethod' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'reimbursement.administration.payment.method.get.by.id', 'uses' => 'ReimbursementAdministrationController@getPaymentMethodByID' ] );
            Route::post( 'post', [ 'as' => 'reimbursement.administration.payment.method.post', 'uses' => 'ReimbursementAdministrationController@postPaymentMethod' ] );
            Route::get( 'table', [ 'as' => 'admin.payment.method.table', 'uses' => 'ReimbursementAdministrationController@paymentMethodDataTable' ] );
        });

        Route::group( ['prefix' => 'employee-reimbursement' ], function() {
            Route::get( '/', [ 'as' => 'reimbursement.administration.employee.reimbursement', 'uses' => 'ReimbursementAdministrationController@employeeReimbursement' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'reimbursement.administration.employee.reimbursement.delete', 'uses' => 'ReimbursementAdministrationController@destroyPaymentMethod' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'reimbursement.administration.employee.reimbursement.get.by.id', 'uses' => 'ReimbursementAdministrationController@getPaymentMethodByID' ] );
            Route::post( 'post', [ 'as' => 'reimbursement.administration.employee.reimbursement.post', 'uses' => 'ReimbursementAdministrationController@postPaymentMethod' ] );
            Route::get( 'table', [ 'as' => 'admin.employee.reimbursement.table', 'uses' => 'ReimbursementAdministrationController@employeeReimbursementDataTable' ] );
        });

    });

    Route::group( ['prefix' => 'master' ], function() {


        Route::group( ['prefix' => 'metadata' ], function() {
            Route::group( ['prefix' => 'imigration-status' ], function() {
                Route::get( '/', [ 'as' => 'metadata.imigration.status', 'uses' => 'MetadataController@imigrationStatus' ] );
                Route::delete( 'delete/{id}', [ 'as' => 'metadata.imigration.status.delete', 'uses' => 'MetadataController@destroyImigrationStatus' ] );
                Route::get( 'get-by-id/{id}', [ 'as' => 'metadata.imigration.status.get.by.id', 'uses' => 'MetadataController@getImigrationStatusByID' ] );
                Route::post( 'post', [ 'as' => 'metadata.imigration.status.post', 'uses' => 'MetadataController@postImigrationStatus' ] );
                Route::get( 'table', [ 'as' => 'admin.imigration.status.table', 'uses' => 'MetadataController@imigrationStatusDataTable' ] );
            });

            Route::group( ['prefix' => 'ethnicity' ], function() {
                Route::get( '/', [ 'as' => 'metadata.ethnicity', 'uses' => 'MetadataController@ethnicity' ] );
                Route::delete( 'delete/{id}', [ 'as' => 'metadata.ethnicity.delete', 'uses' => 'MetadataController@destroyEthnicity' ] );
                Route::get( 'get-by-id/{id}', [ 'as' => 'metadata.ethnicity.get.by.id', 'uses' => 'MetadataController@getEthnicityByID' ] );
                Route::post( 'post', [ 'as' => 'metadata.ethnicity.post', 'uses' => 'MetadataController@postEthnicity' ] );
                Route::get( 'table', [ 'as' => 'admin.ethnicity.table', 'uses' => 'MetadataController@ethnicityDataTable' ] );
            });

            Route::group( ['prefix' => 'nationality' ], function() {
                Route::get( '/', [ 'as' => 'metadata.nationality', 'uses' => 'MetadataController@nationality' ] );
                Route::delete( 'delete/{id}', [ 'as' => 'metadata.nationality.delete', 'uses' => 'MetadataController@destroyNationality' ] );
                Route::get( 'get-by-id/{id}', [ 'as' => 'metadata.nationality.get.by.id', 'uses' => 'MetadataController@getNationalityByID' ] );
                Route::post( 'post', [ 'as' => 'metadata.nationality.post', 'uses' => 'MetadataController@postNationality' ] );
                Route::get( 'table', [ 'as' => 'admin.nationality.table', 'uses' => 'MetadataController@nationalityDataTable' ] );
            });
        });

        Route::group( ['prefix' => 'region' ], function() {
            Route::group( ['prefix' => 'country' ], function() {
                Route::get( '/', [ 'as' => 'master.region.country', 'uses' => 'RegionController@country' ] );
                Route::delete( 'delete/{id}', [ 'as' => 'master.region.country.delete', 'uses' => 'RegionController@destroyCountry' ] );
                Route::get( 'get-by-id/{id}', [ 'as' => 'master.region.country.get.by.id', 'uses' => 'RegionController@getCountryByID' ] );
                Route::post( 'post', [ 'as' => 'master.region.country.post', 'uses' => 'RegionController@postCountry' ] );
                Route::get( 'table', [ 'as' => 'master.region.country.table', 'uses' => 'RegionController@countryDataTable' ] );
            });

            Route::group( ['prefix' => 'province' ], function() {
                Route::get( '/', [ 'as' => 'master.region.province', 'uses' => 'RegionController@province' ] );
                Route::delete( 'delete/{id}', [ 'as' => 'master.region.province.delete', 'uses' => 'RegionController@destroyProvince' ] );
                Route::get( 'get-by-id/{id}', [ 'as' => 'master.region.province.get.by.id', 'uses' => 'RegionController@getProvinceByID' ] );
                Route::post( 'post', [ 'as' => 'master.region.province.post', 'uses' => 'RegionController@postProvince' ] );
                Route::get( 'table', [ 'as' => 'master.region.province.table', 'uses' => 'RegionController@provinceDataTable' ] );
            });

            Route::group( ['prefix' => 'city' ], function() {
                Route::get( '/', [ 'as' => 'master.region.city', 'uses' => 'RegionController@city' ] );
                Route::delete( 'delete/{id}', [ 'as' => 'master.region.city.delete', 'uses' => 'RegionController@destroyCity' ] );
                Route::get( 'get-by-id/{id}', [ 'as' => 'master.region.city.get.by.id', 'uses' => 'RegionController@getCityByID' ] );
                Route::post( 'post', [ 'as' => 'master.region.city.post', 'uses' => 'RegionController@postCity' ] );
                Route::post( 'get-province-by-country', [ 'as' => 'master.region.get.province.by.country', 'uses' => 'RegionController@getProvinceByCountry' ] );
                Route::get( 'table', [ 'as' => 'master.region.city.table', 'uses' => 'RegionController@cityDataTable' ] );
            });
        });
    });

    Route::group( ['prefix' => 'qualification-setup' ], function() {

        Route::group( ['prefix' => 'skill' ], function() {
            Route::get( '/', [ 'as' => 'qualification.setup.skill', 'uses' => 'QualificationSetupController@skill' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'qualification.setup.skill.delete', 'uses' => 'QualificationSetupController@destroySkill' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'qualification.setup.skill.get.by.id', 'uses' => 'QualificationSetupController@getSkillByID' ] );
            Route::post( 'post', [ 'as' => 'qualification.setup.skill.post', 'uses' => 'QualificationSetupController@postSkill' ] );
            Route::get( 'table', [ 'as' => 'admin.skill.table', 'uses' => 'QualificationSetupController@skillDataTable' ] );
        });
        Route::group( ['prefix' => 'education' ], function() {
            Route::get( '/', [ 'as' => 'qualification.setup.education', 'uses' => 'QualificationSetupController@education' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'qualification.setup.education.delete', 'uses' => 'QualificationSetupController@destroyEducation' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'qualification.setup.education.get.by.id', 'uses' => 'QualificationSetupController@getEducationByID' ] );
            Route::post( 'post', [ 'as' => 'qualification.setup.education.post', 'uses' => 'QualificationSetupController@postEducation' ] );
            Route::get( 'table', [ 'as' => 'admin.education.table', 'uses' => 'QualificationSetupController@educationDataTable' ] );
        });
        Route::group( ['prefix' => 'certification' ], function() {
            Route::get( '/', [ 'as' => 'qualification.setup.certification', 'uses' => 'QualificationSetupController@certification' ] );
            Route::delete( 'delete/{id}', [ 'as' => 'qualification.setup.certification.delete', 'uses' => 'QualificationSetupController@destroyCertification' ] );
            Route::get( 'get-by-id/{id}', [ 'as' => 'qualification.setup.certification.get.by.id', 'uses' => 'QualificationSetupController@getCertificationByID' ] );
            Route::post( 'post', [ 'as' => 'qualification.setup.certification.post', 'uses' => 'QualificationSetupController@postCertification' ] );
            Route::get( 'table', [ 'as' => 'admin.certification.table', 'uses' => 'QualificationSetupController@certificationDataTable' ] );
        });
    });


    //prototype route and its temporary bro
    // Event Management
    Route::group( ['middleware' => 'sentinel_access:user','prefix' => 'prototype'], function()  {
        Route::get( 'attendance-setup', [ 'as' => 'prototype.attendance.setup', 'uses' => 'PrototypeController@attendanceSetup' ] );
        Route::get( 'company-loans', [ 'as' => 'prototype.company.loans', 'uses' => 'PrototypeController@companyLoans' ] );
        Route::get( 'company-structure', [ 'as' => 'prototype.company.structure', 'uses' => 'PrototypeController@companyStructure' ] );
        Route::get( 'job-detail-setup', [ 'as' => 'prototype.job.detail.setup', 'uses' => 'PrototypeController@jobDetailSetup' ] );
        Route::get( 'qualification-setup', [ 'as' => 'prototype.qualification.setup', 'uses' => 'PrototypeController@qualificationSetup' ] );
        Route::get( 'training-setup', [ 'as' => 'prototype.training.setup', 'uses' => 'PrototypeController@trainingSetup' ] );
        Route::get( 'leave-settings', [ 'as' => 'prototype.leave.setting', 'uses' => 'PrototypeController@leaveSetting' ] );
        Route::get( 'attendance', [ 'as' => 'prototype.attendance', 'uses' => 'PrototypeController@attendance' ] );
        Route::get( 'reimbursment', [ 'as' => 'prototype.reimbursment', 'uses' => 'PrototypeController@reimbursment' ] );
        Route::get( 'salary', [ 'as' => 'prototype.salary', 'uses' => 'PrototypeController@salary' ] );
        Route::get( 'reimbursment-employees', [ 'as' => 'prototype.reimbursment.employee', 'uses' => 'PrototypeController@reimbursmentEmployee' ] );

    });


    // Dashboard
    Route::get( 'dashboard', ['middleware' => 'sentinel_auth','as' => 'admin.dashboard.index', 'uses' => 'DashboardController@index'] );

    // Event Management
    Route::group( ['middleware' => 'sentinel_access:event','prefix' => 'event'], function()  {
        Route::get( 'getData', [ 'as' => 'admin.event.getdata', 'uses' => 'EventController@getData' ] );
        Route::get( '/' , [ 'as' => 'admin.event.index', 'uses' => 'EventController@index' ]);
        Route::get( 'create' , [ 'as' => 'admin.event.create', 'uses' => 'EventController@create' ]);
        Route::post( 'store' , [ 'as' => 'admin.event.store', 'uses' => 'EventController@store' ]);
        Route::get( 'edit/{id}' , [ 'as' => 'admin.event.edit', 'uses' => 'EventController@edit' ]);
        Route::put( 'update/{id}' , [ 'as' => 'admin.event.update', 'uses' => 'EventController@update' ]);
        Route::delete( 'destroy/{id}' , [ 'as' => 'admin.event.destroy', 'uses' => 'EventController@destroy' ]);

        // ajax
        Route::any( 'ajax' , [ 'as' => 'admin.event.ajax', 'uses' => 'EventController@ajaxRequest' ]);
    });


    Route::group( ['prefix' => 'administration'], function() {
        // Route::group( ['prefix' => 'general-setting'], function () {
        //     Route::get( '', ['as' => 'admin.setting.index', 'uses' => 'SettingController@index'] );
        //     Route::post( 'update', ['as' => 'admin.setting.update', 'uses' => 'SettingController@update'] );
        // });
        Route::group( ['middleware' => 'sentinel_access:user','prefix' => 'user'], function () {
            Route::get( 'getData', [ 'as' => 'admin.user.getdata', 'uses' => 'UserController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.user.index', 'uses' => 'UserController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.user.create', 'uses' => 'UserController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.user.store', 'uses' => 'UserController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.user.edit', 'uses' => 'UserController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.user.update', 'uses' => 'UserController@update' ]);
            Route::put( 'update-password/{id}' , [ 'as' => 'admin.user.update_password', 'uses' => 'UserController@updatePassword' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.user.destroy', 'uses' => 'UserController@destroy' ]);
        });
        //
        // Role Management
        Route::group( ['middleware' => 'sentinel_access:role','prefix' => 'role'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.role.getdata', 'uses' => 'RoleController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.role.index', 'uses' => 'RoleController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.role.create', 'uses' => 'RoleController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.role.store', 'uses' => 'RoleController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.role.edit', 'uses' => 'RoleController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.role.update', 'uses' => 'RoleController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.role.destroy', 'uses' => 'RoleController@destroy' ]);
        });

        // Menu Management
        Route::group( ['middleware' => 'sentinel_access:menu','prefix' => 'menu'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.menu.getdata', 'uses' => 'MenuController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.menu.index', 'uses' => 'MenuController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.menu.create', 'uses' => 'MenuController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.menu.store', 'uses' => 'MenuController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.menu.edit', 'uses' => 'MenuController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.menu.update', 'uses' => 'MenuController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.menu.destroy', 'uses' => 'MenuController@destroy' ]);
        });
    });

    Route::group( ['prefix' => 'master'], function()  {

        // Sponsors Management
        Route::group( ['middleware' => 'sentinel_access:','prefix' => 'sponsors'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.sponsor.getdata', 'uses' => 'SponsorController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.sponsor.index', 'uses' => 'SponsorController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.sponsor.create', 'uses' => 'SponsorController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.sponsor.store', 'uses' => 'SponsorController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.sponsor.edit', 'uses' => 'SponsorController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.sponsor.update', 'uses' => 'SponsorController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.sponsor.destroy', 'uses' => 'SponsorController@destroy' ]);
        });

        // Skills Management
        Route::group( ['middleware' => 'sentinel_access:','prefix' => 'skill'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.skill.getdata', 'uses' => 'SkillController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.skill.index', 'uses' => 'SkillController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.skill.create', 'uses' => 'SkillController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.skill.store', 'uses' => 'SkillController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.skill.edit', 'uses' => 'SkillController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.skill.update', 'uses' => 'SkillController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.skill.destroy', 'uses' => 'SkillController@destroy' ]);
        });

        // Facilitators Management
        Route::group( ['middleware' => 'sentinel_access:','prefix' => 'facilitators'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.facilitators.getdata', 'uses' => 'FacilitatorController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.facilitators.index', 'uses' => 'FacilitatorController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.facilitators.create', 'uses' => 'FacilitatorController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.facilitators.store', 'uses' => 'FacilitatorController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.facilitators.edit', 'uses' => 'FacilitatorController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.facilitators.update', 'uses' => 'FacilitatorController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.facilitators.destroy', 'uses' => 'FacilitatorController@destroy' ]);
        });

        // Country Management
        Route::group( ['middleware' => 'sentinel_access:country','prefix' => 'area/country'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.country.getdata', 'uses' => 'CountryController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.country.index', 'uses' => 'CountryController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.country.create', 'uses' => 'CountryController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.country.store', 'uses' => 'CountryController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.country.edit', 'uses' => 'CountryController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.country.update', 'uses' => 'CountryController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.country.destroy', 'uses' => 'CountryController@destroy' ]);
        });


        // Region Management
        Route::group( ['middleware' => 'sentinel_access:region','prefix' => 'area/region'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.region.getdata', 'uses' => 'RegionController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.region.index', 'uses' => 'RegionController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.region.create', 'uses' => 'RegionController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.region.store', 'uses' => 'RegionController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.region.edit', 'uses' => 'RegionController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.region.update', 'uses' => 'RegionController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.region.destroy', 'uses' => 'RegionController@destroy' ]);
        });

        // Cities Management
        Route::group( ['middleware' => 'sentinel_access:city','prefix' => 'area/city'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.city.getdata', 'uses' => 'CityController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.city.index', 'uses' => 'CityController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.city.create', 'uses' => 'CityController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.city.store', 'uses' => 'CityController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.city.edit', 'uses' => 'CityController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.city.update', 'uses' => 'CityController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.city.destroy', 'uses' => 'CityController@destroy' ]);

            // ajax
            Route::any( 'ajax' , [ 'as' => 'admin.city.ajax', 'uses' => 'CityController@ajaxRequest' ]);
        });

        // EventType Management
        Route::group( ['middleware' => 'sentinel_access:event_type','prefix' => 'event-type'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.event_type.getdata', 'uses' => 'EventTypeController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.event_type.index', 'uses' => 'EventTypeController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.event_type.create', 'uses' => 'EventTypeController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.event_type.store', 'uses' => 'EventTypeController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.event_type.edit', 'uses' => 'EventTypeController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.event_type.update', 'uses' => 'EventTypeController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.event_type.destroy', 'uses' => 'EventTypeController@destroy' ]);
        });

        // Banner Management
        Route::group( ['middleware' => 'sentinel_access:banner','prefix' => 'banner'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.banner.getdata', 'uses' => 'BannerController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.banner.index', 'uses' => 'BannerController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.banner.create', 'uses' => 'BannerController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.banner.store', 'uses' => 'BannerController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.banner.edit', 'uses' => 'BannerController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.banner.update', 'uses' => 'BannerController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.banner.destroy', 'uses' => 'BannerController@destroy' ]);
        });

        // Article Management
        Route::group( ['middleware' => 'sentinel_access:article','prefix' => 'article'], function()  {
            Route::get( 'getData', [ 'as' => 'admin.article.getdata', 'uses' => 'ArticleController@getData' ] );
            Route::get( '/' , [ 'as' => 'admin.article.index', 'uses' => 'ArticleController@index' ]);
            Route::get( 'create' , [ 'as' => 'admin.article.create', 'uses' => 'ArticleController@create' ]);
            Route::post( 'store' , [ 'as' => 'admin.article.store', 'uses' => 'ArticleController@store' ]);
            Route::get( 'edit/{id}' , [ 'as' => 'admin.article.edit', 'uses' => 'ArticleController@edit' ]);
            Route::put( 'update/{id}' , [ 'as' => 'admin.article.update', 'uses' => 'ArticleController@update' ]);
            Route::delete( 'destroy/{id}' , [ 'as' => 'admin.article.destroy', 'uses' => 'ArticleController@destroy' ]);
            Route::post('/upload', [ 'as' => 'upload_elfinder', 'uses' => 'ArticleController@upload']);
        });
    });

    Route::group( ['prefix' => 'setting'], function()  {
      Route::group( ['middleware' => 'sentinel_access:general','prefix' => 'general'], function()  {
        Route::get( '/' , [ 'as' => 'admin.setting.index', 'uses' => 'SettingController@index' ]);
        Route::post( 'update' , [ 'as' => 'admin.setting.update', 'uses' => 'SettingController@update' ]);
      });
    });

    Route::group( ['prefix' => 'log'], function()  {
      //Log Eror view
      Route::get( 'error-logs', [ 'as' => 'admin.logs.error', 'uses' => 'LogController@error' ] );

      //Log Eror data
      Route::get('logs', ['as' => 'admin.logs.report', 'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index']);

      //Activity Log
      Route::get('activity-log', ['as' => 'admin.logs.activity', 'uses' => 'LogController@activity']);
      Route::get('log-ajax', ['as' => 'admin.logs.ajax', 'uses' => 'LogController@ajax']);

    });
});
