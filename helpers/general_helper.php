<?php
/* -----------------------------------------------------
 | Function Helpers.
 | -----------------------------------------------------
 |
 | Create basic function to easier developing
 */

use App\Models\Menu;
use App\Models\Role;

 if (! function_exists('admin_check')) {
    /**
     * Check admin page permission.
     *
     * @param  string $permission
     * @return boolean
     */
    function admin_check( $permission )
    {
        if ( $user = Sentinel::check() ) {
            if( ! $user->isSuperAdmin() ) {
                if ( ! Sentinel::hasAnyAccess( $permission ) ) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}

if (! function_exists('csv_to_array')) {
    /**
     * Check admin page permission.
     *
     * @param  string $permission
     * @return boolean
     */
    function csv_to_array($filename='', $header)
    {
        $delimiter=',';
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                $data[] = array_combine(array_intersect_key($header, $row), array_intersect_key($row, $header));
            }
            fclose($handle);
        }

        return $data;
    }
}


if (! function_exists('convertDateJMY')) {
    /**
     * Convert date.
     *
     * @param  date
     * @return converted date
     */
    function convertDateJMY($date)
    {
        if($date != NULL )
            return date("j M Y H:i:s", strtotime($date));
        else
            return '';
    }
}

// costum
function define_child($parent_id) {
    $child = Menu::Where('parent_id',$parent_id)->where('is_active',TRUE)->get();

    return $child;
}

function permission_val($id,$permission)
{
    $role = Role::find($id);
    $format = json_decode(json_encode($role),true);

    $result = (isset($format['permissions'][$permission]) && $format['permissions'][$permission] != ''  ? 1 : 0);
    return $result;
}
