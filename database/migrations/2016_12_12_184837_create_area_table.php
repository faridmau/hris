<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('countries', function (Blueprint $table) {
          $table->increments('id');
          $table->string( 'name' )->nullable();
          $table->string('slug' )->nullable();
          $table->string('iso_code')->nullable();
          $table->boolean( 'is_active' )->default(true);

          $table->softDeletes();
          $table->timestamps();
      });

      Schema::create('provinces', function (Blueprint $table) {
        $table->increments( 'id' );
        $table->string( 'name' )->nullable();
        $table->string( 'slug' )->nullable();
        $table->boolean( 'is_active' )->default(true);
        $table->integer( 'country_id' )->unsigned()->index();

        $table->softDeletes();
        $table->timestamps();
      });

      Schema::create('cities', function (Blueprint $table) {
        $table->increments( 'id' );
        $table->integer( 'province_id' )->unsigned()->index();
        $table->integer( 'country_id' )->unsigned()->index();
        $table->string( 'name' )->nullable();
        $table->string( 'slug' )->nullable();
        $table->boolean( 'is_active' )->default(true);

        $table->softDeletes();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'provinces' );
        Schema::dropIfExists( 'cities' );
        Schema::dropIfExists('countries');
    }
}
