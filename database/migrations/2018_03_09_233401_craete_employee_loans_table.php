<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CraeteEmployeeLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'author_id' );
            $table->integer( 'employee_id' );
            $table->integer( 'loan_type_id' );
            $table->double( 'amount' );
            $table->integer( 'tenor' );
            $table->date( 'start_date' );
            $table->date( 'end_date' );
            $table->double( 'monthly_installment' );
            $table->text( 'description' )->nullable();
            $table->enum( 'status',['Approved', 'Paid', 'Rejected', 'Waiting', 'Suspended'] );
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_loans');
    }
}
