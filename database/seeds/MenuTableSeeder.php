<?php

use Illuminate\Database\Seeder;

use App\Models\Menu;

class MenuTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('menus')->truncate();

    $menus = [
      // parent_id = 1
      [
        'parent_id' => 0,
        'name'      => 'Dashboard',
        'icon'      => 'fa-home',
        'url'       => 'dashboard',
        'is_active' => true
      ],
      // parent_id = 2
      [
        'parent_id' => 0,
        'name'      => 'Administration',
        'icon'      => 'fa-gears',
        'url'       => '#',
        'is_active' => true
      ],
      // parent_id = 3
      [
        'parent_id' => 0,
        'name'      => 'Data Master',
        'icon'      => 'fa-database',
        'url'       => '#',
        'is_active' => true
      ],
      // parent_id = 4
      [
        'parent_id' => 0,
        'name'      => 'Event',
        'icon'      => 'fa-ticket',
        'url'       => 'event',
        'is_active' => true
      ],
      // parent_id = 5
      [
        'parent_id' => 0,
        'name'      => 'Setting',
        'icon'      => 'fa-th-large',
        'url'       => '#',
        'is_active' => true
      ],
      // parent_id = 6
      [
        'parent_id' => 0,
        'name'      => 'Systems',
        'icon'      => 'fa-gears',
        'url'       => '#',
        'is_active' => true
      ],
      // parent_id = 7
      [
        'parent_id' => 0,
        'name'      => 'Employees',
        'icon'      => 'fa-user-circle',
        'url'       => '#',
        'is_active' => true
      ],
      // parent_id = 8
      [
        'parent_id' => 0,
        'name'      => 'Finance',
        'icon'      => 'fa-money',
        'url'       => '#',
        'is_active' => true
      ],
      // parent_id = 9
      [
        'parent_id' => 0,
        'name'      => 'Report',
        'icon'      => 'fa-file-excel-o',
        'url'       => '#',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'User',
        'icon'      => '',
        'url'       => 'administration/user',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Role',
        'icon'      => '',
        'url'       => 'administration/role',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Menu',
        'icon'      => '',
        'url'       => 'administration/menu',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Job Detail Setup',
        'icon'      => '',
        'url'       => 'job-setup/job-title',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Reimbursment',
        'icon'      => '',
        'url'       => 'reimbursement/type',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Qualification Setup',
        'icon'      => '',
        'url'       => 'qualification-setup/skill',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Training Setup',
        'icon'      => '',
        'url'       => 'prototype/training-setup',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Company Structures',
        'icon'      => '',
        'url'       => 'prototype/company-structure',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Leave Settings',
        'icon'      => '',
        'url'       => 'prototype/leave-settings',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Company Loans',
        'icon'      => '',
        'url'       => 'company-loan/loan-type',
        'is_active' => true
      ],
      [
        'parent_id' => 2,
        'name'      => 'Attendance Setup',
        'icon'      => '',
        'url'       => 'prototype/attendance-setup',
        'is_active' => true
      ],
      [
        'parent_id' => 3,
        'name'      => 'Region',
        'icon'      => '',
        'url'       => 'master/region/country',
        'is_active' => true
      ],
      [
        'parent_id' => 3,
        'name'      => 'Metadata',
        'icon'      => '',
        'url'       => 'master/metadata/imigration-status',
        'is_active' => true
      ],
      [
        'parent_id' => 6,
        'name'      => 'Activity Log',
        'icon'      => '',
        'url'       => 'log/activity-log',
        'is_active' => true
      ],
      [
        'parent_id' => 6,
        'name'      => 'Audit Log',
        'icon'      => '',
        'url'       => 'system/audit-log',
        'is_active' => true
      ],
      [
        'parent_id' => 6,
        'name'      => 'General Settings',
        'icon'      => '',
        'url'       => 'system/general-setting',
        'is_active' => true
      ],
      [
          'parent_id' => 6,
          'name'      => 'Log Error',
          'icon'      => '',
          'url'       => 'system/error-log',
          'is_active' => true
      ],
      [
          'parent_id' => 7,
          'name'      => 'Employees',
          'icon'      => '',
          'url'       => 'prototype/list-employees',
          'is_active' => true
      ],
      [
          'parent_id' => 7,
          'name'      => 'Document Management',
          'icon'      => '',
          'url'       => 'prototype/document-management-employees',
          'is_active' => true
      ],
      [
          'parent_id' => 7,
          'name'      => 'Reimbursment',
          'icon'      => '',
          'url'       => 'prototype/reimbursment-employees',
          'is_active' => true
      ],
      [
          'parent_id' => 7,
          'name'      => 'Monitor Attendances',
          'icon'      => '',
          'url'       => 'prototype/monitor-attendance-employees',
          'is_active' => true
      ],
      [
          'parent_id' => 8,
          'name'      => 'Loans',
          'icon'      => '',
          'url'       => 'prototype/loans',
          'is_active' => true
      ],
      [
          'parent_id' => 8,
          'name'      => 'Reimbursment',
          'icon'      => '',
          'url'       => 'prototype/reimbursment',
          'is_active' => true
      ],
      [
          'parent_id' => 8,
          'name'      => 'Salary',
          'icon'      => '',
          'url'       => 'prototype/salary',
          'is_active' => true
      ],
      [
          'parent_id' => 9,
          'name'      => 'Reimbursment Report',
          'icon'      => '',
          'url'       => 'prototype/reimbursment',
          'is_active' => true
      ],
      [
          'parent_id' => 9,
          'name'      => 'Salary Report',
          'icon'      => '',
          'url'       => 'prototype/reimbursment',
          'is_active' => true
      ],
      [
          'parent_id' => 9,
          'name'      => 'Cashbon Report',
          'icon'      => '',
          'url'       => 'prototype/cashbon',
          'is_active' => true
      ],
      [
          'parent_id' => 9,
          'name'      => 'Attendance Report',
          'icon'      => '',
          'url'       => 'prototype/attendance',
          'is_active' => true
      ],
      [
          'parent_id' => 9,
          'name'      => 'Loan Report',
          'icon'      => '',
          'url'       => 'prototype/loan',
          'is_active' => true
      ],
    ];

    foreach ( $menus as $key => $data ) {
      Menu::create( $data );
    }
  }
}
