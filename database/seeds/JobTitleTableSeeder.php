<?php

use Illuminate\Database\Seeder;

use App\Models\JobTitle;

class JobTitleTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('job_titles')->truncate();

    $types = [
      [
        'code' => 'PM',
        'name' => 'Project Manager',
        'description'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'spesification'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'slug'      => 'project-manager',
        'is_active'      => 1
      ],
      [
        'code' => 'BA',
        'name' => 'Business Analyst',
        'description'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'spesification'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'slug'      => 'business-analyst',
        'is_active'      => 1
      ],
      [
        'code' => 'QE',
        'name' => 'QA Enginer',
        'description'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'spesification'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'slug'      => 'qa-enginer',
        'is_active'      => 1
      ],
      [
        'code' => 'PC',
        'name' => 'Project Coordinator',
        'description'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'spesification'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'slug'      => 'project-coordinator',
        'is_active'      => 1
      ],
    ];

    foreach ( $types as $key => $data ) {
      JobTitle::create( $data );
    }
  }
}
