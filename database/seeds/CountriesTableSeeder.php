<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();
         $data = [
        		'name'      			=> 'Indonesia',
        		'slug'      => 'indonesia',
        		'created_at'      => date('Y-m-d'),
        		'updated_at'      => date('Y-m-d'),
            'iso_code'    => 'IDN',
            'is_active' => true
        	];
        $country = Country::insertGetId($data);
    }
}
