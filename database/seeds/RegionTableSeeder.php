<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('provinces')->truncate();
      $country = Country::first();

        Excel::filter('chunk')->load(public_path('csv/provinces.csv'))->chunk(250, function($results) use($country){
            $header = [ 'id', 'name','country_id'];
            foreach ($results->toArray() as $row) {
                array_push($row,$country->id);
                $data = array_combine($header, $row);

                DB::table( 'provinces' )->insert($data);
            }
        });
    }
}
