<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(MenuTableSeeder::class);
         $this->call(CountriesTableSeeder::class);
         $this->call(RegionTableSeeder::class);
         $this->call(CityTableSeeder::class);
         $this->call(SettingTableSeeder::class);
         $this->call(JobTitleTableSeeder::class);
         $this->call(LoanTypeTableSeeder::class);
    }
}
