<?php

use Illuminate\Database\Seeder;

use App\Models\LoanType;

class LoanTypeTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('loan_types')->truncate();

    $types = [
      [
        'name' => 'Personal Loan',
        'description'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'slug'      => 'personal-loan',
        'is_active'      => 1
      ],
      [
        'name' => 'Education Loan',
        'description'      => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
        'slug'      => 'education-loan',
        'is_active'      => 1
      ]
    ];

    foreach ( $types as $key => $data ) {
      LoanType::create( $data );
    }
  }
}
