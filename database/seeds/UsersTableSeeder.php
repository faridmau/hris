<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();
        DB::table('users')->truncate();

        $datas = [
            [
                'email' => 'admin@hris.apps',
                'username' => 'superadmin112233',
                'name' => 'Super Administrator',
                'gender' => 'Men',
                'role' => 'super-admin'
            ],
            [
                'email' => 'johndoe@hris.apps',
                'username' => 'admin112233',
                'name' => 'John Doe',
                'gender' => 'Men',
                'role' => 'admin'
            ],
            [
                'email' => 'member@ems.com',
                'username' => 'member112233',
                'name' => 'Member',
                'gender' => 'Men',
                'role' => 'member'
            ]
        ];

        foreach ( $datas as $key => $data ) {
            $user = Sentinel::registerAndActivate( [
                'email' => $data[ 'email' ],
                'username' => $data[ 'username' ],
                'password' => User::DEFAULT_PASSWORD,
                'name' => $data[ 'name' ],
                'gender' => $data[ 'gender' ]
            ] );
            Sentinel::findRoleBySlug( $data[ 'role' ] )->users()->attach( $user );
        }

        $faker = \Faker\Factory::create();
        for ($i = 2; $i <= 5; $i++) {
            $user = Sentinel::registerAndActivate( [
                'email' => $faker->freeEmail,
                'username' => $faker->userName,
                'password' => User::DEFAULT_PASSWORD,
                'name' => $faker->firstName . ' ' . $faker->lastName,
                'gender' => 'Women'
            ] );

            Sentinel::findRoleBySlug('admin')->users()->attach( $user );
        }
        Schema::enableForeignKeyConstraints();
    }
}
