<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::first();
        Schema::disableForeignKeyConstraints();
        DB::table('cities')->truncate();
        Schema::enableForeignKeyConstraints();
        Excel::filter('chunk')->load(public_path('csv/cities.csv'))->chunk(250, function($results) use($country){
            $header = [ 'id', 'province_id', 'name','country_id'];
            foreach ($results->toArray() as $row) {
                array_push($row,$country->id);
                $data = array_combine($header, $row);

                DB::table( 'cities' )->insert($data);
            }
        });
    }
}
