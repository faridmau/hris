<?php

use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('districts')->truncate();
        Schema::enableForeignKeyConstraints();
        Excel::filter('chunk')->load(public_path('csv/districts.csv'))->chunk(250, function($results) {
            $header = [ 'id', 'city_id', 'name'];
            foreach ($results->toArray() as $row) {
                $data = array_combine($header, $row);

                DB::table( 'districts' )->insert($data);
            }
        });
    }
}
