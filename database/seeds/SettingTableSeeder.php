<?php

use Illuminate\Database\Seeder;

use App\Models\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        $datas = [
          [
            'app_name' => 'Default web',
            'description' => 'Default descriptrion',
            'email_support' => 'info@support.com',
            'phone_support' => '+62 2 123 123 (EXT. 422)',
            'longitude' => '110.37231509999992',
            'lattitude' => '-6.987269599999999',
            'address' => 'Jalan Siliwangi, Kembangarum, Semarang City, Central Java, Indonesia',
            'keywords' => '',
            'tagline' => '',
            'icon' => 'icon.png',
            'logo' => 'logo.png'
          ]
        ];
        foreach ($datas as $data) {
          Setting::create($data);
        }

    }
}
