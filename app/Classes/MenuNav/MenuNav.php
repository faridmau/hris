<?php
/* -----------------------------------------------------
 | MenuNav
 | -----------------------------------------------------
 |
 | Create basic function to easier developing
 */

    namespace App\Classes\MenuNav;

    use Illuminate\Http\Request;
    use App\Models\Menu;
    use Sentinel;

    class MenuNav {

        /*Start For Backend Navigation*/
        public function menu(  ) {
            $menus = Menu::where( 'is_active', 1 )->where('parent_id', 0)->get();
            $data = [];
            foreach ($menus as $i => $menu) {
                $childs = Menu::where('parent_id', $menu->id)->orderBy('name', 'ASC')->get()->toArray();

                $result = array(
                    'id' => $menu->id,
                    'name' => $menu->name,
                    'url' => $menu->url,
                    'slug' => $menu->slug,
                    'icon' => $menu->icon,
                    'parent_id' => $menu->parent_id,
                    'child'     => $childs,
                    );

                array_push($data, $result);
            }

            return $data;
        }

        public function mainMenu( $url ) {

            $menus = $this->menu();
            $user = Sentinel::check();
            if (!$user) {
                return redirect()->route('auth.login');
            }

            $html = '<ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">';
            // $html .= '<li data-menu="" class=" nav-item">';
            // $html .= '<a href="'.route('admin.dashboard.index').'" data-toggle="" class="nav-link"><i class="ft-home"></i>';
            // $html .= '<span>Dashboard</span>';
            // $html .= '</a>';
            // $html .= '</li>';

            foreach ($menus as $i => $menu) {
                if ($menu['url'] == '#' || empty($menu['url']) ) {
                    $parent = true;
                    $url_menu = '#';
                } else {
                    $parent = false;
                    $url_menu = $url .'/'. $menu['url'];
                }
                if ($user->hasAnyAccess([$menu['slug'],'admin'])  || $menu['slug'] == 'dashboard') {
                    $html .= '<li '.($parent == true ? 'data-menu="dropdown"' : "").' class="'.($parent == true ? "dropdown" : '').' nav-item '.(\Request::is('*'.$menu["slug"].'') ? "active":"").'"  data-menu="dropdown" >
                        <a href=" '. $url_menu .' " data-toggle="dropdown" class="nav-link">
                        <i class="site-menu-icon fa '. $menu['icon'] .'" aria-hidden="true"></i>
                        <span class="menu-title">'. $menu['name'] .'</span>
                        <span class="site-menu-arrow"></span></a>';
                        if (count($menu['child']) > 0) {
                            $html .= '<ul class="dropdown-menu">';
                            foreach ($menu['child'] as $j => $child) {
                            if ($user->hasAnyAccess([$child['slug'],'admin']) || $menu['slug'] == 'dashboard') {
                                $html .= '<li>
                                        <a class="dropdown-item" href=" '. $url . '/'. $child['url'] .' ">
                                            <span class="site-menu-title">'. $child['name'] .'</span>
                                        </a>
                                    </li>';
                                }
                            }
                            $html .= '</ul>';
                        }
                        $html .= '</li>';
                    }
            }
            return $html;
        }
    }
