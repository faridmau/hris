<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use Image;

class ProfileController extends Controller
{


    /**
     * Display a dashboard page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$activity = Sentinel::getUser()->activity;
        $id = Sentinel::getUser();

        return view('backend.admin.profile.index', compact('activity','id'));
    }


}
