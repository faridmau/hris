<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{


    /**
     * Display a home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.content.homepage');
    }

    /**
     * Display a about page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('frontend.content.about');
    }


    /**
     * Display a contact page.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('frontend.content.contact');
    }

    /**
     * Display a blog page.
     *
     * @return \Illuminate\Http\Response
     */
    public function blog()
    {
        return view('frontend.content.blog');
    }

    /**
     * Display a sponsors page.
     *
     * @return \Illuminate\Http\Response
     */
    public function sponsors()
    {
        return view('frontend.content.sponsors');
    }

    /**
     * Display a facilitator page.
     *
     * @return \Illuminate\Http\Response
     */
    public function facilitator()
    {
        return view('frontend.content.facilitator');
    }

        /**
     * Display a facilitator detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function facilitatorDetail()
    {
        return view('frontend.content.facilitator-detail');
    }


    /**
     * Display a events page.
     *
     * @return \Illuminate\Http\Response
     */
    public function events()
    {
        return view('frontend.content.event');
    }

    /**
     * Display a faq page.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq()
    {
        return view('frontend.content.faq');
    }

    /**
     * Display a testimoni page.
     *
     * @return \Illuminate\Http\Response
     */
    public function testimoni()
    {
        return view('frontend.content.testimoni');
    }


    /**
     * Display a ticket page.
     *
     * @return \Illuminate\Http\Response
     */
    public function ticket()
    {
        return view('frontend.content.ticket');
    }


    /**
     * Display a gallery page.
     *
     * @return \Illuminate\Http\Response
     */
    public function gallery()
    {
        return view('frontend.content.gallery');
    }

}
