<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;

use Validator;
use Sentinel;
use Mail;
use Reminder;

class AuthController extends Controller
{
    /**
     * Display a login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('backend.auth.login');
    }

    /**
     * Display a signup page.
     *
     * @return \Illuminate\Http\Response
     */
    public function signup()
    {
        return view('backend.auth.signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'email'     => 'required',
            'password'  => 'required'
            ]);

        if ( $validation->fails() ) {
            flash()->error( 'Please insert valid Input!' );
            return redirect()->back()->withInput()->withErrors( $validation );
        } else {
            
            try {
                $remember =  (bool) $request->input( 'remember_me' );
                $login = Sentinel::authenticate([
                    'login'    => $request->email,
                    'password' => $request->password,
                ],$remember);

                if ( ! $login ) {
                    flash()->error( 'Wrong email or username!' );
                    return redirect()->back()->withInput()->withErrors( $validation->messages() );
                }

                flash()->success('Login success! Welcome to admin page!');
                return redirect()->route('admin.dashboard.index');
            } catch (\Exception $e) {
                flash()->error( 'Error login!' . $e );
                return redirect()->back()->withInput()->withErrors( $validation->messages() );
            }
        }
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function forgot()
    {
        return view('backend.auth.forgot_password');
    }

    /**
     * Sends a forgot.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return \Illuminate\Http\Response
     */
    public function sendForgot(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors( $validation );
        }

        if ( $user = Sentinel::findByCredentials( [ 'login' => $request->email ] ) ) {
            if ( ! $reminder = Reminder::exists( $user ) ) {
                $reminder = Reminder::create( $user );
            } else {
                echo "Failed";
            }
            $date = date('d-m-Y');
            $today = date("j M Y", strtotime($date));
            
            $data = [
                'email' => $user->email,
                'name'  => $user->name,
                'subject' => 'Reset Your Password',
                'code'  => $reminder->code,
                'today' => $today,
                'id'    => $user->id
            ];

            $mail = Mail::to( $request->email )->send(new ForgotPassword($data));

            flash()->success('Email Send. Please check your inbox to reset your password!');
            return redirect()->route('auth.login');
        } else {
            return redirect()->back()->withInput()->withErrors( ['error' => 'Email is not registered!'] );
        }
    }

    /**
     * Display page reset password
     *
     * @param      int  $id     The identifier
     * @param      string  $code   The code
     *
     * @return \Illuminate\Http\Response
     */
    public function resetPassword($id, $code)
    {
        $user = Sentinel::findById($id);

        if ( Reminder::exists($user, $code) ) {
            return view( 'backend.auth.reset_password', compact('id', 'code') );
        } else {
            return "gagal";
        }
    }

    /**
     * Update old Password
     *
     * @param  \Illuminate\Http\Request  $request  The request
     *
     */
    public function updatePassword(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'password' => 'min:6|required',
            'password_confirmation' => 'min:6|same:password'
            ]);

        if ($validation->fails()) {
             return redirect()->back()->withInput()->withErrors( $validation );
        }

        try {
            $user = Sentinel::findById($request->id);
            $reminder = Reminder::exists($user, $request->code);

            if ($reminder == false) {
                return redirect()->route('auth.login');
            }

            Reminder::complete($user, $request->code, $request->password);

            flash()->success('Success to change password');

            return redirect()->route('auth.login');
        } catch (Exception $e) {
            flash()->error( 'Error Change Password!' . $e );
            return redirect()->back()->withInput()->withErrors( $validation->messages() );
        }
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Sentinel::logout();
        return redirect()->route( 'auth.login' );
    }

}
