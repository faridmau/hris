<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingRequest;

use App\Models\Activity;
use App\Models\User;

use DataTables;
use Carbon\Carbon;

class LogController extends Controller
{
    public function error(Request $request)
    {
       return view('backend.admin.log.error');
    }

    public function activity(Request $request)
    {
      $users = User::pluck('name','id')->all();
      $users[0] = 'Select User';

      ksort($users);
      return view('backend.admin.log.activity',compact('users'));
    }

    public function ajax(Request $request)
    {
      $type = $request->type;
      switch ($type) {
        case 'getDataFilterActivity':
          return $this->generateTableActivity(Activity::log($request->all()));
          break;

        case 'getDataFilterActivityAll':
          return $this->generateTableActivity(Activity::logActivityAll());
          break;

        default:
          return 1;
          break;
      }
    }

    protected function generateTableActivity($data){
      return Datatables::of($data)
        ->addColumn('created_at', function( $activity ) {
          return Carbon::parse($activity->created_at)->format('d F Y');
        })
        ->make(true);
    }
}
