<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MenuRequest;


use App\Models\Menu;

use DataTables;

class MenuController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view( 'backend.admin.menu.index' );
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $parents =  Menu::where('parent_id', 0)->get();
    $menu = array();
    return view( 'backend.admin.menu.create', compact('menu','parents') );
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  App\Http\Admin\MenuRequest  $request
  * @return \Illuminate\Http\Response
  */
  public function store(MenuRequest $request)
  {
    try {
      if (empty( $request->parent_id )) {
        $request->merge( [ 'parent_id' => 0 ] );
      }

      $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
      $menu = Menu::create( $request->all() );

      flash()->success( trans('general.success') );
      return redirect()->route( 'admin.menu.index' );
    } catch (Exception $e) {

      flash()->error( trans('general.error') );
      return back()->withInput();
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $menu = Menu::find($id);

    return view('backend.admin.menu.show', compact('menu'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $parents =  Menu::where('parent_id', 0)->get();
    $menu = Menu::find($id);

    return view('backend.admin.menu.edit', compact('menu','parents'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try {
      $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
      $parent = !empty($request->parent_id) ? $request->parent_id : 0;
      $request->merge( ['parent_id' => $parent] );
      $menu = Menu::find($id)->update( $request->all() );

      flash()->success( trans('general.success') );
      return redirect()->route( 'admin.menu.index' );
    } catch (Exception $e) {

      flash()->error( trans('general.error') );
      return back()->withInput();
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    try {

      $menu = Menu::where('id', $id)->delete();

      flash()->success( trans('general.destroy-success') );
      return redirect()->route( 'admin.menu.index' );
    } catch (Exception $e) {

      flash()->error( trans('general.destroy-error') );
      return back()->withInput();
    }
  }

  /**
  * Datatables for Menu Managament
  * @return  string
  */
  public function getData()
  {
    return DataTables::of(Menu::datatables())
    ->rawColumns(['parent_id','action'])
    ->make(true);
  }
}
