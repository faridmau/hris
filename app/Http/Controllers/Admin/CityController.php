<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CityRequest;

use App\Models\City;
use App\Models\Region;
use App\Models\Country;
use DataTables;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.city.index' );
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::where( 'is_active', 1 )->orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
        return view( 'backend.admin.city.create', compact( 'countries' ) );
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $city = City::create( $request->all() );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.city.index' );

        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = City::find($id);

        return view( 'backend.admin.city.show' );
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::find($id);
        $regions = Region::where('country_id',$city->country_id)->where( 'is_active', 1 )->orderBy( 'name', 'ASC' )->get()->pluck( 'name', 'id' )->all();
        $countries = Country::where( 'is_active', 1 )->orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
        return view( 'backend.admin.city.edit', compact( 'city', 'regions','countries') );
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $city = City::find($id)->update( $request->all() );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.city.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $city = City::find($id);
            $city->delete();
            flash()->success( trans('general.destroy-success') );

            return redirect()->route( 'admin.city.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    /**
     * DataTables for Role Managament
     * @return  string
     */
    public function getData()
    {
        return DataTables::of(City::datatables())
        ->addColumn( 'action', function ( $city ) {
            $edit_url = route('admin.city.edit', $city->id );
            $delete_url = route('admin.city.destroy', $city->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $city ) {
            return $city->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function ajaxRequest(Request $request){
      $result = array();

      $region = Region::where('country_id',$request->country_id)->where( 'is_active', 1 )->orderBy( 'name', 'ASC' )->pluck( 'name', 'id' )->all();;
      if (count($region) > 0) {
        $result = $region;
      }

      return $result;
    }
}
