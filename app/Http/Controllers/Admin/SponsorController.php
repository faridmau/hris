<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SponsorRequest;

use Illuminate\Http\Request;
use App\Models\Sponsor;
use DataTables;

class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.sponsor.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'backend.admin.sponsor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SponsorRequest $request)
    {
        $requests = $request->except('_token');
        try {
            if($request->has('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $filename = 'Sponsor-'. $request->input('name'). md5(time() + rand(0,100)) . '.' . $image->getClientOriginalExtension();
                $image->move('uploads/sponsor',$filename);
                $requests['image'] = 'uploads/sponsor/' . $filename;
            }
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            $sponsor= Sponsor::create( $requests );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.sponsor.index' );
        } catch (Exception $e) {
            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sponsor = Sponsor::find($id);
        return view( 'backend.admin.sponsor.edit', compact( 'sponsor' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requests = $request->except('_token');
        try {
            if($request->has('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $filename = 'Sponsor-' . md5(time() + rand(0,100)) . '.' . $image->getClientOriginalExtension();
                $image->move('uploads/sponsor',$filename);
                $requests['image'] = 'uploads/sponsor/' . $filename;
            }
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            $sponsor= Sponsor::find( $id )->update($requests);

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.sponsor.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $sponsor = Sponsor::find($id);
            $sponsor->delete();
            flash()->success( trans('general.destroy-success') );

            return redirect()->route( 'admin.sponsor.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(Sponsor::datatables())
        ->addColumn( 'action', function ( $sponsor ) {
            $edit_url = route('admin.sponsor.edit', $sponsor->id );
            $delete_url = route('admin.sponsor.destroy', $sponsor->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('image' , function ($sponsor) {
            return '<div class="row"> <div class="col-md-12"><a href="#" class="thumbnail"><img src="'.asset('').$sponsor->image .'" alt="'.$sponsor->name.'" style="width:100%;"></a></div></div>';
        })
        ->addColumn('is_active', function( $sponsor ) {
            return $sponsor->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action','image'])
        ->make(true);
    }
}
