<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SkillRequest;
use App\Http\Requests\Admin\EducationRequest;
use App\Http\Requests\Admin\CertificationRequest;
use Illuminate\Http\Request;

use App\Models\Skill;
use App\Models\Education;
use App\Models\Certification;
use DataTables;
use Sentinel;
use DB;

class QualificationSetupController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function skill()
    {
        return view( 'backend.admin.qualification_setup.skill' );
    }

    public function education()
    {
        return view( 'backend.admin.qualification_setup.education' );
    }

    public function certification()
    {
        return view( 'backend.admin.qualification_setup.certification' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postSkill(SkillRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            if($request->input('id')) {
                $id = $request->input('id');
                $skill = Skill::find($id)->update( $requests );
            } else {
                $skill= Skill::create( $requests );
            }
            if($skill) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('qualification.setup.skill');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postEducation(EducationRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $education = Education::find($id)->update( $requests );
            } else {
                $education= Education::create( $requests );
            }
            if($education) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('qualification.setup.education');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postCertification(CertificationRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $certification = Certification::find($id)->update( $requests );
            } else {
                $certification= Certification::create( $requests );
            }
            if($certification) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('qualification.setup.certification');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getSkillByID($id)
    {
        $skill = Skill::find($id);
        if($skill) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['skill'] = $skill;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getEducationByID($id)
    {
        $education = Education::find($id);
        if($education) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['education'] = $education;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getCertificationByID($id)
    {
        $certification = Certification::find($id);
        if($certification) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['certification'] = $certification;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroySkill($id)
    {
        DB::BeginTransaction();
        try {
            $skill = Skill::find($id);
            $skill->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'qualification.setup.skill' );
    }

    public function destroyEducation($id)
    {
        DB::BeginTransaction();
        try {
            $education = Education::find($id);
            $education->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'qualification.setup.education' );
    }

    public function destroyCertification($id)
    {
        DB::BeginTransaction();
        try {
            $certification = Certification::find($id);
            $certification->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'qualification.setup.certification' );
    }

    public function skillDataTable()
    {
        return DataTables::of(Skill::datatables())
        ->addColumn( 'action', function ( $skill ) {
            $edit_url = route('qualification.setup.skill.get.by.id', $skill->id );
            $delete_url = route('qualification.setup.skill.delete', $skill->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $skill->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $skill ) {
            return $skill->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function educationDataTable()
    {
        return DataTables::of(Education::datatables())
        ->addColumn( 'action', function ( $education ) {
            $edit_url = route('qualification.setup.education.get.by.id', $education->id );
            $delete_url = route('qualification.setup.education.delete', $education->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $education->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $education ) {
            return $education->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function certificationDataTable()
    {
        return DataTables::of(Certification::datatables())
        ->addColumn( 'action', function ( $certification ) {
            $edit_url = route('qualification.setup.certification.get.by.id', $certification->id );
            $delete_url = route('qualification.setup.certification.delete', $certification->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $certification->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $certification ) {
            return $certification->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

}
