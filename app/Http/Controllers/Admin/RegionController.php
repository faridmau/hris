<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CountryRequest;
use App\Http\Requests\Admin\ProvinceRequest;
use App\Http\Requests\Admin\CityRequest;
use Illuminate\Http\Request;

use App\Models\Country;
use App\Models\Province;
use App\Models\City;
use DataTables;
use Sentinel;
use DB;

class RegionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function country()
    {
        return view( 'backend.admin.master.region.country' );
    }

    public function province()
    {
        $countries = Country::dropDown();
        return view( 'backend.admin.master.region.province', compact('countries') );
    }

    public function city()
    {
        $countries = Country::dropDown();
        return view( 'backend.admin.master.region.city', compact('countries') );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCountry(CountryRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            if($request->input('id')) {
                $id = $request->input('id');
                $country = Country::find($id)->update( $requests );
            } else {
                $country= Country::create( $requests );
            }
            if($country) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('master.region.country');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postProvince(ProvinceRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $province = Province::find($id)->update( $requests );
            } else {
                $province= Province::create( $requests );
            }
            if($province) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('master.region.province');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postCity(CityRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $city = City::find($id)->update( $requests );
            } else {
                $city= City::create( $requests );
            }
            if($city) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('master.region.city');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCountryByID($id)
    {
        $country = Country::find($id);
        if($country) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['country'] = $country;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getProvinceByID($id)
    {
        $province = Province::find($id);
        if($province) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['province'] = $province;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getCityByID($id)
    {
        $city = City::find($id);
        if($city) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['city'] = $city;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getProvinceByCountry(Request $request)
    {
        $country_id = $request->input('country_id');
        $provinces = Province::dropDown($country_id);
        if($provinces) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['provinces'] = $provinces;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_get_data');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCountry($id)
    {
        DB::BeginTransaction();
        try {
            $country = Country::find($id);
            $country->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'master.region.country' );
    }

    public function destroyProvince($id)
    {
        DB::BeginTransaction();
        try {
            $province = Province::find($id);
            $province->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'master.region.province' );
    }

    public function destroyCity($id)
    {
        DB::BeginTransaction();
        try {
            $city = City::find($id);
            $city->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'master.region.city' );
    }

    public function countryDataTable()
    {
        return DataTables::of(Country::datatables())
        ->addColumn( 'action', function ( $country ) {
            $edit_url = route('master.region.country.get.by.id', $country->id );
            $delete_url = route('master.region.country.delete', $country->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $country->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $country ) {
            return $country->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function provinceDataTable()
    {
        return DataTables::of(Province::datatables())
        ->addColumn( 'action', function ( $province ) {
            $edit_url = route('master.region.province.get.by.id', $province->id );
            $delete_url = route('master.region.province.delete', $province->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $province->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('country', function( $province ) {
            return $province->country->name;
        })
        ->addColumn('is_active', function( $province ) {
            return $province->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function cityDataTable()
    {
        return DataTables::of(City::datatables())
        ->addColumn( 'action', function ( $city ) {
            $edit_url = route('master.region.city.get.by.id', $city->id );
            $delete_url = route('master.region.city.delete', $city->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $city->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('province', function( $city ) {
            return $city->province;
        })
        ->addColumn('coutry', function( $city ) {
            return $city->country;
        })
        ->addColumn('is_active', function( $city ) {
            return $city->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

}
