<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ImigrationStatusRequest;
use App\Http\Requests\Admin\EthnicityRequest;
use App\Http\Requests\Admin\NationalityRequest;
use Illuminate\Http\Request;

use App\Models\ImigrationStatus;
use App\Models\Ethnicity;
use App\Models\Nationality;
use DataTables;
use Sentinel;
use DB;

class MetadataController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imigrationStatus()
    {
        return view( 'backend.admin.master.metadata.imigration_status' );
    }

    public function ethnicity()
    {
        return view( 'backend.admin.master.metadata.ethnicity' );
    }

    public function nationality()
    {
        return view( 'backend.admin.master.metadata.nationality' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postImigrationStatus(ImigrationStatusRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            if($request->input('id')) {
                $id = $request->input('id');
                $imigrationStatus = ImigrationStatus::find($id)->update( $requests );
            } else {
                $imigrationStatus= ImigrationStatus::create( $requests );
            }
            if($imigrationStatus) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('metadata.imigration.status');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postEthnicity(EthnicityRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $ethnicity = Ethnicity::find($id)->update( $requests );
            } else {
                $ethnicity= Ethnicity::create( $requests );
            }
            if($ethnicity) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('metadata.ethnicity');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postNationality(NationalityRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $nationality = Nationality::find($id)->update( $requests );
            } else {
                $nationality= Nationality::create( $requests );
            }
            if($nationality) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('metadata.nationality');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getImigrationStatusByID($id)
    {
        $imigrationStatus = ImigrationStatus::find($id);
        if($imigrationStatus) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['imigrationStatus'] = $imigrationStatus;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getEthnicityByID($id)
    {
        $ethnicity = Ethnicity::find($id);
        if($ethnicity) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['ethnicity'] = $ethnicity;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getNationalityByID($id)
    {
        $nationality = Nationality::find($id);
        if($nationality) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['nationality'] = $nationality;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyImigrationStatus($id)
    {
        DB::BeginTransaction();
        try {
            $imigrationStatus = ImigrationStatus::find($id);
            $imigrationStatus->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'metadata.imigration.status' );
    }

    public function destroyEthnicity($id)
    {
        DB::BeginTransaction();
        try {
            $ethnicity = Ethnicity::find($id);
            $ethnicity->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'metadata.ethnicity' );
    }

    public function destroyNationality($id)
    {
        DB::BeginTransaction();
        try {
            $nationality = Nationality::find($id);
            $nationality->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'metadata.nationality' );
    }

    public function imigrationStatusDataTable()
    {
        return DataTables::of(ImigrationStatus::datatables())
        ->addColumn( 'action', function ( $imigrationStatus ) {
            $edit_url = route('metadata.imigration.status.get.by.id', $imigrationStatus->id );
            $delete_url = route('metadata.imigration.status.delete', $imigrationStatus->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $imigrationStatus->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $imigrationStatus ) {
            return $imigrationStatus->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function ethnicityDataTable()
    {
        return DataTables::of(Ethnicity::datatables())
        ->addColumn( 'action', function ( $ethnicity ) {
            $edit_url = route('metadata.ethnicity.get.by.id', $ethnicity->id );
            $delete_url = route('metadata.ethnicity.delete', $ethnicity->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $ethnicity->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $ethnicity ) {
            return $ethnicity->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function nationalityDataTable()
    {
        return DataTables::of(Nationality::datatables())
        ->addColumn( 'action', function ( $nationality ) {
            $edit_url = route('metadata.nationality.get.by.id', $nationality->id );
            $delete_url = route('metadata.nationality.delete', $nationality->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $nationality->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $nationality ) {
            return $nationality->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

}
