<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventTypeRequest;

use App\Models\Event;
use App\Models\EventType;
use DataTables;
use Excel;

class EventTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.event_type.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'backend.admin.event_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventTypeRequest $request)
    {
        try {
            $event_type = EventType::create( $request->all() );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.event_type.index' );

        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event_type = EventType::find($id);

        return view( 'backend.admin.event_type.show' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $event_type = EventType::find($id);
        return view( 'backend.admin.event_type.edit',compact('event_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventTypeRequest $request, $id)
    {
        try {
            $event_type = EventType::find($id)->update($request->all());

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.event_type.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          if (Event::where('event_type_id', $id)->count() > 0) {
              flash()->error( trans('general.destroy-error') );
              return redirect()->route( 'admin.event_type.index' );
          } else {
            $event_type = EventType::find($id)->delete();
            flash()->success( trans('general.destroy-success') );
            return redirect()->route( 'admin.event_type.index' );
          }
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(EventType::datatables())
        ->addColumn( 'action', function ( $event_type ) {
            $edit_url = route('admin.event_type.edit', $event_type->id );
            $delete_url = route('admin.event_type.destroy', $event_type->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $menu ) {
            return $menu->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }
}
