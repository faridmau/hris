<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BannerRequest;

use Illuminate\Http\Request;
use App\Models\Banner;
use DataTables;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.banner.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'backend.admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $requests = $request->except('_token');
        try {
            if($request->has('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $filename = 'Banner-' . md5(time() + rand(0,100)) . '.' . $image->getClientOriginalExtension();
                $image->move('uploads/banner',$filename);
                $requests['image'] = 'uploads/banner/' . $filename;
            }
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            $banner= Banner::create( $request );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.banner.index' );
        } catch (Exception $e) {
            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view( 'backend.admin.banner.edit', compact( 'banner' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requests = $request->except('_token');
        try {
            if($request->has('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $filename = 'Banner-' . md5(time() + rand(0,100)) . '.' . $image->getClientOriginalExtension();
                $image->move('uploads/banner',$filename);
                $requests['image'] = 'uploads/banner/' . $filename;
            }
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            $banner= Banner::find( $id )->update($requests);

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.banner.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $banner = Banner::find($id);
            $banner->delete();
            flash()->success( trans('general.destroy-success') );

            return redirect()->route( 'admin.banner.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(Banner::datatables())
        ->addColumn( 'action', function ( $banner ) {
            $edit_url = route('admin.banner.edit', $banner->id );
            $delete_url = route('admin.banner.destroy', $banner->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('image' , function ($banner) {
            return '<a href="'. asset('') . $banner->image .'">'. $banner->image .'</a>';
        })
        ->addColumn('is_active', function( $banner ) {
            return $banner->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action','image'])
        ->make(true);
    }
}
