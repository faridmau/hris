<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ArticleRequest;
use Illuminate\Http\Request;

use App\Models\Article;
use DataTables;
use Sentinel;
use DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.article.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'backend.admin.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        DB::BeginTransaction();
        try {
            $request = $request->except('_token');
            $request['author_id'] = Sentinel::getUser()->id;
            if(empty($request['is_draft'])) $request['posted_date'] = DB::raw('now()');
    
            Article::create($request);
        } catch (Exception $e) {
            flash()->error( trans('general.error') );
            return back()->withInput();
        }
        DB::commit();
        flash()->success( trans('general.success') );
        return redirect()->route('admin.article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        return view( 'backend.admin.article.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        DB::BeginTransaction();
        try {
            $request = $request->except('_token','_method');
            if(empty($request['is_draft'])) $request['posted_date'] = DB::raw('now()');
            $article = Article::find($id);
            $article->update($request);
        } catch (Exception $e) {
            flash()->error( trans('general.error') );
            return back()->withInput();
        }
        DB::Commit();
        flash()->success( trans('general.success') );
        return redirect()->route( 'admin.article.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::BeginTransaction();
        try {
            $article = Article::find($id);
            $article->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'admin.article.index' );
    }

    public function getData()
    {
        return DataTables::of(Article::datatables())
        ->addColumn( 'action', function ( $article ) {
            $edit_url = route('admin.article.edit', $article->id );
            $delete_url = route('admin.article.destroy', $article->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_draft', function( $article ) {
            return $article->is_draft == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->addColumn('author_id',function( $article ) {
            return $article->users()->first()->name;
        })
        ->rawColumns(['is_draft','action'])
        ->make(true);
    }

    public function upload(Request $request) {
        $mimeAllowed = ["image/png" => null,"image/jpeg" => null,"image/jpg" => null];
        $mimeFile = $request->file('upload')->getMimeType();
        $validate = array_has($mimeAllowed, $mimeFile);
        $funcNum = $request->get('CKEditorFuncNum');
        $url = "";

        if ($request->file('upload')->isValid() && $validate) {
            $destinationPath = 'uploads/ckeditor'; // upload path
            $extension = $request->file('upload')->getClientOriginalExtension(); // getting image extension
            $fileName = sha1(time()).'.'.$extension; // renameing image
            $request->file('upload')->move($destinationPath, $fileName); // uploading file to given path
            $url = '/' . $destinationPath . '/' . $fileName;
            $message = "Upload file successfull";
        } else {
            $funcNum = $request->get('CKEditorFuncNum');
            $message = "Upload file failed !";
        }
        return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$funcNum.'", "' . $url .'", "' . $message .'");</script>';
    }

}
