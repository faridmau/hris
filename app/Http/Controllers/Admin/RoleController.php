<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\RoleRequest;
use App\Models\Role;
use App\Models\Menu;

use DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Sentinel::check();
        return view('backend.admin.roles.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Role::getListPermission();
        return view('backend.admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        try {
            foreach ($request->permissions as $key => $value) {
                $temp[$key] = ($value == 1 ? TRUE:FALSE);
            }

            $request['permissions'] = $temp;
            $role = Role::create($request->all());

            flash()->success( trans( 'general.success', [
                'attribute' => trans( 'island.role' ),
                'detail' => '#' . $role->id . ' | ' . $role->slug
            ] ) );

            return redirect()->route( 'admin.role.index' );
        } catch (Exception $e) {
            flash()->error( trans( 'general.destroy-error', [
                'attribute' => trans( 'island.role' ),
            ] ) );

            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Role::getListPermission();
        $menu = Menu::get();
        return view('backend.admin.roles.edit', compact('role','permissions','menu'));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
          if (!$request->permissions) {
            $temp = array();
          }else{
            foreach ($request->permissions as $key => $value) {
              $temp[$key] = ($value == 1 ? TRUE:FALSE);
            }
          }

            $request['permissions'] = $temp;
            Role::find($id)->update( $request->all() );
            $role = Role::find($id);
            flash()->success( trans( 'general.success', [
                'attribute' => trans( 'island.role' ),
                'detail' => '#' . $role->id . ' | ' . $role->slug
            ] ) );

            return redirect()->route( 'admin.role.index' );
        } catch (Exception $e) {
            flash()->error( trans( 'general.destroy-error', [
                'attribute' => trans( 'island.role' ),
            ] ) );

            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $role = Role::findOrFail($id)->delete();

            flash()->success( trans( 'general.destroy-success' ) );

            return redirect()->route( 'admin.role.index' );
        } catch (Exception $e) {
            flash()->error( trans( 'general.destroy-error', [
                'attribute' => trans( 'island.role' ),
            ] ) );

            return back();
        }
    }

    /**
     * DataTables for Role Managament
     * @return  string
     */
    public function getData()
    {
        return DataTables::of(Role::datatables())
        ->addColumn( 'action', function ( $role ) {
            $user = \Sentinel::check();
            if ($user->hasAnyAccess(['admin'])) {
                $edit_url = route('admin.role.edit', $role->id );
                $delete_url = route('admin.role.destroy', $role->id);

                $data['edit_url']   = $edit_url;
                $data['delete_url'] = $delete_url;

                return view('backend.admin.forms.action', $data);
            } else {
                return 'No Have Access';
            }
        })
        ->make(true);
    }
}
