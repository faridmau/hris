<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{


    /**
     * Display a dashboard page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('backend.admin.dashboard.index');
    }


}
