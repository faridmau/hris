<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FacilitatorRequest;

use App\Models\Facilitator;
use App\Models\Skill;
use DataTables;

class FacilitatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.facilitator.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $skills = Skill::orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      $facilitator = array();
      return view( 'backend.admin.facilitator.create',compact('facilitator','skills'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FacilitatorRequest $request)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $request['skills'] = json_encode($request->skills);
            $facilitator = Facilitator::create( $request->except('image') );

            if ($request->has('photo') && $request->file('photo')->isValid()) {
                $photo = $request->file('photo');
                $name = $facilitator->id . '-' . $facilitator->name . '-'  . date("Y-m-d H:i:s") . '.' . $photo->getClientOriginalExtension();

                $photo->move('uploads/facilitator', $name);
                $facilitator->photo = 'uploads/facilitator/' . $name;
                $facilitator->update();
            }

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.facilitators.index' );

        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $facilitator = Facilitator::find($id);

        return view( 'backend.admin.facilitator.show' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $facilitator = Facilitator::find($id);
      $skills = Skill::orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      return view( 'backend.admin.facilitator.edit',compact('facilitator','skills'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FacilitatorRequest $request, $id)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $request['skills'] = json_encode($request->skills);
            $facilitator = Facilitator::find($id)->update( $request->except('image') );

            if ($request->has('photo') && $request->file('photo')->isValid()) {
                $photo = $request->file('photo');
                $name = $facilitator->id . '-' . $facilitator->name . '-'  . date("Y-m-d H:i:s") . '.' . $photo->getClientOriginalExtension();

                $photo->move('uploads/facilitator', $name);
                $facilitator->photo = 'uploads/facilitator/' . $name;
                $facilitator->update();
            }

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.facilitators.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $facilitator = Facilitator::find($id)->delete();
            flash()->success( trans('general.destroy-success') );
            return redirect()->route( 'admin.facilitators.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(Facilitator::datatables())
        ->addColumn( 'action', function ( $facilitator ) {
            $edit_url = route('admin.facilitators.edit', $facilitator->id );
            $delete_url = route('admin.facilitators.destroy', $facilitator->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $menu ) {
            return $menu->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }
}
