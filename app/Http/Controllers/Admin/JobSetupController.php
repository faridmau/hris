<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\JobTitleRequest;
use App\Http\Requests\Admin\PayGradeRequest;
use App\Http\Requests\Admin\EmploymentStatusRequest;
use Illuminate\Http\Request;

use App\Models\JobTitle;
use App\Models\PayGrade;
use App\Models\EmploymentStatus;
use DataTables;
use Sentinel;
use DB;

class JobSetupController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jobTitle()
    {
        return view( 'backend.admin.job_setup.job_title' );
    }

    public function payGrade()
    {
        return view( 'backend.admin.job_setup.pay_grade' );
    }

    public function employmentStatus()
    {
        return view( 'backend.admin.job_setup.employment_status' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postJobTitle(JobTitleRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            if($request->input('id')) {
                $id = $request->input('id');
                $job_title = JobTitle::find($id)->update( $requests );
            } else {
                $job_title= JobTitle::create( $requests );
            }
            if($job_title) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('job.setup.job.title');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postPayGrade(PayGradeRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $pay_grade = PayGrade::find($id)->update( $requests );
            } else {
                $pay_grade= PayGrade::create( $requests );
            }
            if($pay_grade) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('job.setup.pay.grade');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postEmploymentStatus(EmploymentStatusRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $employment_status = EmploymentStatus::find($id)->update( $requests );
            } else {
                $employment_status= EmploymentStatus::create( $requests );
            }
            if($employment_status) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('job.setup.employment.status');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getJobTitleByID($id)
    {
        $job_title = JobTitle::find($id);
        if($job_title) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['job_title'] = $job_title;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getPayGradeByID($id)
    {
        $pay_grade = PayGrade::find($id);
        if($pay_grade) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['pay_grade'] = $pay_grade;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getEmploymentStatusByID($id)
    {
        $employment_status = EmploymentStatus::find($id);
        if($employment_status) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['employment_status'] = $employment_status;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyJobTitle($id)
    {
        DB::BeginTransaction();
        try {
            $job_title = JobTitle::find($id);
            $job_title->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'job.setup.job.title' );
    }

    public function destroyPayGrade($id)
    {
        DB::BeginTransaction();
        try {
            $pay_grade = PayGrade::find($id);
            $pay_grade->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'job.setup.pay.grade' );
    }

    public function destroyEmploymentStatus($id)
    {
        DB::BeginTransaction();
        try {
            $employment_status = EmploymentStatus::find($id);
            $employment_status->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'job.setup.employment.status' );
    }

    public function jobTitleDataTable()
    {
        return DataTables::of(JobTitle::datatables())
        ->addColumn( 'action', function ( $job_title ) {
            $edit_url = route('job.setup.job.title.get.by.id', $job_title->id );
            $delete_url = route('job.setup.job.title.delete', $job_title->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $job_title->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $job_title ) {
            return $job_title->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function payGradeDataTable()
    {
        return DataTables::of(PayGrade::datatables())
        ->addColumn( 'action', function ( $pay_grade ) {
            $edit_url = route('job.setup.pay.grade.get.by.id', $pay_grade->id );
            $delete_url = route('job.setup.pay.grade.delete', $pay_grade->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $pay_grade->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $pay_grade ) {
            return $pay_grade->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function employmentStatusDataTable()
    {
        return DataTables::of(EmploymentStatus::datatables())
        ->addColumn( 'action', function ( $employment_status ) {
            $edit_url = route('job.setup.employment.status.get.by.id', $employment_status->id );
            $delete_url = route('job.setup.employment.status.delete', $employment_status->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $employment_status->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $employment_status ) {
            return $employment_status->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

}
