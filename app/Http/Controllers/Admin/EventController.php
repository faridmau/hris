<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventRequest;

use App\Models\Event;
use App\Models\EventType;
use App\Models\Country;
use App\Models\Region;
use App\Models\City;
use DataTables;
use Excel;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.event.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $countries = Country::where( 'is_active', 1 )->orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      $eventTypes = EventType::orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      $event = array();
      return view( 'backend.admin.event.create',compact('event','countries','eventTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        try {
            $request['is_published'] = (isset($request->is_published) ? $request->is_published:0);
            $event = Event::create( $request->except('image') );

            if ($request->has('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $name = $event->id . '-' . $event->name . '-'  . date("Y-m-d H:i:s") . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/event', $name);
                $event->image = 'uploads/event/' . $name;
                $event->update();
            }

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.event.index' );

        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);

        return view( 'backend.admin.event.show' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $event = Event::find($id);
      $countries = Country::where( 'is_active', 1 )->orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      $regions = Region::where( 'is_active', 1 )->orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      $cities = City::where( 'is_active', 1 )->orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
      $eventTypes = EventType::orderBy('name', 'ASC')->get()->pluck( 'name', 'id' )->all();
        return view( 'backend.admin.event.edit',compact('event','countries','regions','cities','eventTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, $id)
    {
        try {
            $request['is_published'] = (isset($request->is_published) ? $request->is_published:0);
            $event = Event::find($id)->update($request->except('image'));

            if ($request->has('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $name = $event->id . '-' . $event->name . '-'  . date("Y-m-d H:i:s") . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/event', $name);
                $event->image = 'uploads/event/' . $name;
                $event->update();
            }

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.event.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $event = Event::find($id)->delete();
            flash()->success( trans('general.destroy-success') );
            return redirect()->route( 'admin.event.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(Event::datatables())
        ->addColumn( 'action', function ( $event ) {
            $edit_url = route('admin.event.edit', $event->id );
            $delete_url = route('admin.event.destroy', $event->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_published', function( $menu ) {
            return $menu->is_published == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_published','action'])
        ->make(true);
    }

    public function ajaxRequest(Request $request){

      $type = $request->type;
      $result = array();
      switch ($type) {
        case 'getRegion':
          $region = Region::where('country_id',$request->country_id)->where( 'is_active', 1 )->orderBy( 'name', 'ASC' )->pluck( 'name', 'id' )->all();;
          if (count($region) > 0) {
            $result = $region;
          }
          break;

        case 'getCity':
          $city = City::where('region_id',$request->region_id)->where( 'is_active', 1 )->orderBy( 'name', 'ASC' )->pluck( 'name', 'id' )->all();;
          if (count($city) > 0) {
            $result = $city;
          }
          break;

        default:
          return $result;
          break;
      }

      return $result;
    }
}
