<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingRequest;

use App\Models\Setting;
use Image;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::first();
        return view('backend.admin.setting.general', compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request)
    {
        try {
            if (Setting::first()) {
                $update = Setting::first();
                $setting = $update->update( $request->except('_token') );
                $setting = Setting::find($update->id);
            } else {
                $setting_id = Setting::insertGetId( $request->except('_token'));
                $setting = Setting::find($setting_id);
            }

            // icon
            if ($request->hasFile( 'icon' )) {

              $image = $request->file( 'icon' );
              $extension = $image->getClientOriginalExtension();
              $uploads = public_path( 'uploads/' );
              if (!file_exists( $uploads )) {
                mkdir( $uploads, 0777 );
              }

              $path = public_path( 'uploads/setting/' );
              if (!file_exists( $path )) {
                mkdir( $path, 0777 );
              }

              $name = 'icon';
              $img  = Image::make($image->getRealPath());
              $img->save($path.$name.'.'.$extension);

              Setting::find($setting->id)->update(['icon' => $name.'.'.$extension]);
            }

            // Logo
            if ($request->hasFile( 'logo' )) {

              $image = $request->file( 'logo' );
              $extension = $image->getClientOriginalExtension();

              $uploads = public_path( 'uploads/' );
              if (!file_exists( $uploads )) {
                mkdir( $uploads, 0777 );
              }

              $path = public_path( 'uploads/setting/' );
              if (!file_exists( $path )) {
                mkdir( $path, 0777 );
              }

              $name = 'logo';
              $img  = Image::make($image->getRealPath());
              $img->save($path.$name.'.'.$extension);

              Setting::find($setting->id)->update(['logo' => $name.'.'.$extension]);
            }

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.setting.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }
}
