<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;

use App\Models\User;
use App\Models\Role;

use DataTables;
use Sentinel;
use Exception;
use Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.user.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles[''] = '- Select Role -';
        foreach(Role::all() as $role) {
            $roles[$role->slug] = $role->name;
        }
        return view( 'backend.admin.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        \DB::BeginTransaction();
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active : 0);
            $user = Sentinel::registerAndActivate( [
                'email' => $request->input('email'),
                'username' => $request->input('username'),
                'password' => $request->input('password'),
                'name' => $request->input('name'),
                'gender' => $request->input('gender'),
                'phone' => $request->input('phone')
            ] );

            Sentinel::findRoleBySlug( $request->input('role') )->users()->attach( $user );
            if ($request->has('avatar') && $request->file('avatar')->isValid()) {
                $avatar = $request->file('avatar');
                $img = Image::make($avatar);
                $cropData = json_decode($request->input('avatar-crop-data'));
                $img->crop((int)$cropData->width,(int)$cropData->height,(int)$cropData->x,(int)$cropData->y);
                $name = $user->id . '-' . $user->username . '-'  . date("Y-m-d H:i:s") . '.' . $avatar->getClientOriginalExtension();
                $img->save('uploads/user/avatar/'.$name);
                $user->image = 'uploads/user/avatar/' . $name;
                $user->update();
            }

            if ($request->has('cover') && $request->file('cover')->isValid()) {
                $cover = $request->file('cover');
                $img = Image::make($cover);
                $cropData = json_decode($request->input('cover-crop-data'));
                $img->crop((int)$cropData->width,(int)$cropData->height,(int)$cropData->x,(int)$cropData->y);
                $name = $user->id . '-' . $user->username . '-'  . date("Y-m-d H:i:s") . '.' . $cover->getClientOriginalExtension();
                $img->save('uploads/user/cover/'.$name);
                $user->cover = 'uploads/user/cover/' . $name;
                $user->update();
            }
        } catch (Exception $e) {
            throw $e;
            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
        \DB::Commit();

        flash()->success( trans('general.success') );
        return redirect()->route( 'admin.user.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $id)
    {
        $roles[''] = '- Select Role -';
        foreach(Role::all() as $role) {
            $roles[$role->slug] = $role->name;
        }
        return view('backend.admin.user.edit', compact('id','roles'));
    }

    public function updateByUser(UpdateUserRequest $request) {
        \DB::begintransaction();
            try {
                $user = Sentinel::getUser();
                Sentinel::update($user,$request->except('image','cover'));
            } catch (Exception $e) {
                throw $e;
            }
        \DB::commit();
        flash()->success( trans('general.success') );
        return redirect()->route( 'profile.index' );
    }

    public function updateAvatarAndCover(Request $request) {
        $request->validate([
            'avatar'     => 'mimes:jpeg,jpg,png|max:4000',
            'cover'     => 'mimes:jpeg,jpg,png|max:4000'
        ]);
        \DB::begintransaction();
            try {
                $user = Sentinel::getUser();

                if ($request->has('avatar') && $request->file('avatar')->isValid()) {
                $avatar = $request->file('avatar');
                $img = Image::make($avatar);
                $cropData = json_decode($request->input('avatar-crop-data'));
                $img->crop((int)$cropData->width,(int)$cropData->height,(int)$cropData->x,(int)$cropData->y);
                $name = $user->id . '-' . $user->username . '-'  . date("Y-m-d H:i:s") . '.' . $avatar->getClientOriginalExtension();
                $img->save('uploads/user/avatar/'.$name);
                $user->image = 'uploads/user/avatar/' . $name;
                $user->update();
            }

            if ($request->has('cover') && $request->file('cover')->isValid()) {
                $cover = $request->file('cover');
                $img = Image::make($cover);
                $cropData = json_decode($request->input('cover-crop-data'));
                $img->crop((int)$cropData->width,(int)$cropData->height,(int)$cropData->x,(int)$cropData->y);
                $name = $user->id . '-' . $user->username . '-'  . date("Y-m-d H:i:s") . '.' . $cover->getClientOriginalExtension();
                $img->save('uploads/user/cover/'.$name);
                $user->cover = 'uploads/user/cover/' . $name;
                $user->update();
            }
            } catch (Exception $e) {
                throw $e;
            }
        \DB::commit();
        flash()->success( trans('general.success') );
        return redirect()->route( 'profile.index' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        \DB::BeginTransaction();
        try {
            $user = Sentinel::findUserById($id);
            $role = Sentinel::findRoleByName($user->roles()->first()->name);
            $role->users()->detach($user);
            $role = Sentinel::findRoleBySlug( $request->input('role') )->users()->attach( $user );
            
            if ($request->has('avatar') && $request->file('avatar')->isValid()) {
                $avatar = $request->file('avatar');
                $img = Image::make($avatar);
                $cropData = json_decode($request->input('avatar-crop-data'));
                $img->crop((int)$cropData->width,(int)$cropData->height,(int)$cropData->x,(int)$cropData->y);

                $name = $user->id . '-' . $user->username . '-'  . date("Y-m-d H:i:s") . '.' . $avatar->getClientOriginalExtension();

                $img->save('uploads/user/avatar/'.$name);

                $user->image = 'uploads/user/avatar/' . $name;
                $user->update();
            }

            if ($request->has('cover') && $request->file('cover')->isValid()) {
                $cover = $request->file('cover');
                $img = Image::make($cover);
                $cropData = json_decode($request->input('cover-crop-data'));

                $img->crop((int)$cropData->width,(int)$cropData->height,(int)$cropData->x,(int)$cropData->y);

                $name = $user->id . '-' . $user->username . '-'  . date("Y-m-d H:i:s") . '.' . $cover->getClientOriginalExtension();

                $img->save('uploads/user/cover/'.$name);

                $user->cover = 'uploads/user/cover/' . $name;
                $user->update();
            }

            Sentinel::update($user,$request->except(['cover','image']));
        } catch (Exception $e) {
            throw $e;
            flash()->error( trans('general.error') );
            return back()->withInput();
        }
        \DB::commit();

        flash()->success( trans('general.success') );
        return redirect()->route( 'admin.user.index' );
    }

    public function updatePasswordByUser(Request $request) {
        if (strlen($request->input('password')) < 6) {
            flash()->error( trans('general.password_min') );
            return back();
        } else if ($request->input('password') == $request->input('password_confirmation')) {
            \DB::begintransaction();
            try {
                $user = Sentinel::getUser()->id;
                $user = User::find($user);
                $user->password = bcrypt($request->input('password'));
                $user->save();
            } catch (Exception $e) {
                throw $e;
            }
            \DB::commit();

            flash()->success( trans('general.success') );
            return redirect()->route('profile.index');
        } else {
            flash()->error( trans('general.password_not_match') );
            return back();
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, User $id)
    {
        if (strlen($request->input('password')) < 6) {
            return response()->json([
                'messages' => 'The password must be at least 6 characters.'
            ]);
        } else if ($request->input('password') == $request->input('password_confirmation')) {
            try {
                $id->password = bcrypt($request->input('password'));
                $id->save();
            } catch (Exception $e) {
                return response()->json([
                    'messages' => $e->getMessage()
                ]);
            }

            return response()->json([
                'messages' => null
            ]);
        } else {
            return response()->json([
                'messages' => 'The password confirmation does not match.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $id)
    {
        try {
            $id->delete();
            flash()->success( trans('general.destroy-success') );
            return redirect()->route( 'admin.user.index' );
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
    }

    /**
  * Datatables for Menu Managament
  * @return  string
  */
  public function getData()
  {
    return DataTables::of(User::datatables())
    ->addColumn( 'action', function ( $user ) {
      $edit_url = route('admin.user.edit', $user->id );
      $delete_url = route('admin.user.destroy', $user->id);
      $edit_password_url = route('admin.user.update_password', $user->id );

      $data['edit_url']   = $edit_url;
      $data['delete_url'] = $delete_url;
      $data['edit_password_url'] = $edit_password_url;

      return view('backend.admin.forms.action', $data);
    })
    ->make(true);
  }
}
