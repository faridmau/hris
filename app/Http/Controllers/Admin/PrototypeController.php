<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrototypeController extends Controller
{


    /**
     * Display a dashboard page.
     *
     * @return \Illuminate\Http\Response
     */
    public function attendanceSetup()
    {
      return view('backend.prototype.attendance_setup');
    }

    public function companyLoans()
    {
      return view('backend.prototype.company_loans');
    }

    public function companyStructure()
    {
      return view('backend.prototype.company_structure');
    }

    public function jobDetailSetup()
    {
      return view('backend.prototype.job_detail_setup');
    }

    public function qualificationSetup()
    {
      return view('backend.prototype.qualification_setup');
    }

    public function attendance()
    {
      return view('backend.prototype.attendance');
    }

    public function trainingSetup()
    {
      return view('backend.prototype.training_setup');
    }

    public function leaveSetting()
    {
      return view('backend.prototype.leave_setting');
    }


    public function reimbursment()
    {
      return view('backend.prototype.reimbursment');
    }

    public function reimbursmentEmployee()
    {
      return view('backend.prototype.reimbursment_employee');
    }

    public function salary()
    {
      return view('backend.prototype.salary');
    }


}
