<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SkillRequest;

use App\Models\Skill;
use App\Models\Region;
use DataTables;
use Excel;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.skill.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'backend.admin.skill.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SkillRequest $request)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $skill = Skill::create( $request->all() );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.skill.index' );

        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $skill = Skill::find($id);

        return view( 'backend.admin.skill.show' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skill = Skill::find($id);
        return view( 'backend.admin.skill.edit', compact('skill') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SkillRequest $request, $id)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $skill = Skill::find($id)->update($request->all());

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.skill.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $skill = Skill::find($id)->delete();
            flash()->success( trans('general.destroy-success') );
            return redirect()->route( 'admin.skill.index' );
        
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(Skill::datatables())
        ->addColumn( 'action', function ( $skill ) {
            $edit_url = route('admin.skill.edit', $skill->id );
            $delete_url = route('admin.skill.destroy', $skill->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $menu ) {
            return $menu->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }
}
