<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CountryRequest;

use App\Models\Country;
use App\Models\Region;
use DataTables;
use Excel;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'backend.admin.country.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'backend.admin.country.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $country = Country::create( $request->all() );

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.country.index' );

        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::find($id);

        return view( 'backend.admin.country.show' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        return view( 'backend.admin.country.edit', compact('country') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, $id)
    {
        try {
            $request['is_active'] = (isset($request->is_active) ? $request->is_active:0);
            $country = Country::find($id)->update($request->all());

            flash()->success( trans('general.success') );
            return redirect()->route( 'admin.country.index' );
        } catch (Exception $e) {

            flash()->error( trans('general.error') );
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (Region::where('country_id', $id)->count() > 0) {
                flash()->error( trans('general.destroy-error') );
                return redirect()->route( 'admin.country.index' );
            } else {
                $country = Country::find($id)->delete();

                flash()->success( trans('general.destroy-success') );
                return redirect()->route( 'admin.country.index' );
            }
        } catch (Exception $e) {

            flash()->error( trans('general.destroy-error') );
            return back()->withInput();
        }
    }

    public function getData()
    {
        return DataTables::of(Country::datatables())
        ->addColumn( 'action', function ( $country ) {
            $edit_url = route('admin.country.edit', $country->id );
            $delete_url = route('admin.country.destroy', $country->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $menu ) {
            return $menu->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Active</label>' : '<label class="badge badge badge-pill badge-warning">Non Active</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }
}
