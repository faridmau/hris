<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ReimbursementTypeRequest;
use App\Http\Requests\Admin\PaymentMethodRequest;
use Illuminate\Http\Request;

use App\Models\ReimbursementType;
use App\Models\PaymentMethod;
use App\Models\User;
use DataTables;
use Sentinel;
use DB;

class ReimbursementAdministrationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reimbursementType()
    {
        return view( 'backend.admin.reimbursement_administration.reimbursement_type' );
    }

    public function paymentMethod()
    {
        return view( 'backend.admin.reimbursement_administration.payment_method');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postReimbursementType(ReimbursementTypeRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
            if($request->input('id')) {
                $id = $request->input('id');
                $reimbursementType = ReimbursementType::find($id)->update( $requests );
            } else {
                $reimbursementType= ReimbursementType::create( $requests );
            }
            if($reimbursementType) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('reimbursement.administration.reimbursement.type');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    public function postPaymentMethod(PaymentMethodRequest $request)
    {
        $requests = $request->except('_token');
        try {
            $requests['is_active'] = (empty($request->input('is_active'))) ? 0 : 1;
           if($request->input('id')) {
                $id = $request->input('id');
                $paymentMethod = PaymentMethod::find($id)->update( $requests );
            } else {
                $paymentMethod= PaymentMethod::create( $requests );
            }
            if($paymentMethod) {
                $status = 200;
                $data['error'] = false;
                $data['title'] = trans('general.title_success');
                $data['message'] = trans('general.success_saved');
                $data['redirect'] = route('reimbursement.administration.payment.method');
            } else {
                $status = 500;
                $data['error'] = true;
                $data['message'] = trans('general.failed_saved');
                $data['title'] = trans('general.title');
            }
            return response()->json($data, $status);
        } catch (Exception $e) {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = 'Error';
            return response()->json($data, $status);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getReimbursementTypeByID($id)
    {
        $reimbursementType = ReimbursementType::find($id);
        if($reimbursementType) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['reimbursementType'] = $reimbursementType;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }

    public function getPaymentMethodByID($id)
    {
        $paymentMethod = PaymentMethod::find($id);
        if($paymentMethod) {
            $status = 200;
            $data['error'] = false;
            $data['title'] = trans('general.title_success');
            $data['paymentMethod'] = $paymentMethod;
        } else {
            $status = 500;
            $data['error'] = true;
            $data['message'] = trans('general.failed_saved');
            $data['title'] = trans('general.title');
        }
        return response()->json($data, $status);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyReimbursementType($id)
    {
        DB::BeginTransaction();
        try {
            $reimbursementType = ReimbursementType::find($id);
            $reimbursementType->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'reimbursement.administration.reimbursement.type' );
    }

    public function destroyPaymentMethod($id)
    {
        DB::BeginTransaction();
        try {
            $paymentMethod = PaymentMethod::find($id);
            $paymentMethod->delete();
        } catch (Exception $e) {
            flash()->error( trans('general.destroy-error') );
            return back();
        }
        DB::Commit();
        flash()->success( trans('general.destroy-success') );
        return redirect()->route( 'reimbursement.administration.payment.method' );
    }

    public function reimbursementTypeDataTable()
    {
        return DataTables::of(ReimbursementType::datatables())
        ->addColumn( 'action', function ( $reimbursementType ) {
            $edit_url = route('reimbursement.administration.reimbursement.type.get.by.id', $reimbursementType->id );
            $delete_url = route('reimbursement.administration.reimbursement.type.delete', $reimbursementType->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $reimbursementType->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $reimbursementType ) {
            return $reimbursementType->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

    public function paymentMethodDataTable()
    {
        return DataTables::of(PaymentMethod::datatables())
        ->addColumn( 'action', function ( $paymentMethod ) {
            $edit_url = route('reimbursement.administration.payment.method.get.by.id', $paymentMethod->id );
            $delete_url = route('reimbursement.administration.payment.method.delete', $paymentMethod->id);

            $data['edit_url']   = $edit_url;
            $data['delete_url'] = $delete_url;
            $data['id'] = $paymentMethod->id;

            return view('backend.admin.forms.action', $data);
        })
        ->addColumn('is_active', function( $paymentMethod ) {
            return $paymentMethod->is_active == 1 ? '<label class="badge badge badge-pill badge-success">Yes</label>' : '<label class="badge badge badge-pill badge-warning">No</label>';
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }

}
