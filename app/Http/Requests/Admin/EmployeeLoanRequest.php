<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeLoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required',
            'loan_type_id' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'amount' => 'required',
            'status' => 'required',
            'tenor' => 'required',
            'monthly_installment' => 'required',
        ];
    }
}
