<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required|email',
            'avatar'    => 'mimes:jpeg,jpg,png|max:4000',
            'cover'     => 'mimes:jpeg,jpg,png|max:4000',
            'phone'     => 'max:13',
        ];
    }
}
