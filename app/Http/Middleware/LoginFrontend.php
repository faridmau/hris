<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class LoginFrontend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $routeName = $request->route()->getName();
      if ( ! Sentinel::check() ) {
           \Session::put('from-uri',$routeName);
           return redirect()->route('frontend.homepage');
       } else {
          return $next($request);
       }
    }
}
