<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class SentinelHasAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string    $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if ( $user = Sentinel::check() ) {
            if( ! $user->isSuperAdmin() && !$user->isAdmin() ) {
                if ( ! Sentinel::hasAccess( $permission ) ) {
                    return redirect()->route( 'admin.dashboard.index' );
                }
            }

            return $next($request);
        } else {
            return redirect()->route( 'auth.login' );
        }
    }
}
