<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class LoginAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if ( $user = Sentinel::check() ) {
            $permission = $user->roles->first()->permissions;
            if (array_key_exists('admin', $permission)) {
                if ($permission['admin'] == false) {
                   return redirect()->route( 'frontend.homepage' );
                }
            }

            return $next($request);
        } else {
            return $next($request);
        }
    }
}
